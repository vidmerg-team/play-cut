/**
 * Viewmodel media spec
 * @module
 */

var vjsHelper = require('video.js/dist/video-js/video.js');
// adds rangeSlider to jquery
require('movie-crop-slider/js/ion.rangeSlider.js');

var mdlMovieCrop = require('movie-crop-slider/js/movie.crop.js');

/**
 * Viewmodel media spec
 * @constructor
 */
var Mdl = function(model) {
  this.getModel = function() {
    return model;
  };

  /**
   * Block: parent of a whole view
   */
  this.block = document.createElement('div');

  /**
   * A wrap of a cutter
   */
  this.cutterWrap = document.createElement('div');

  /**
   * Video js player
   *    generated dynamically
   */
  this.vdo = null;

  this.fillConstPart();
};

/**
 * Fill constant part of a view
 */
Mdl.prototype.fillConstPart = function() {
  this.block.appendChild(this.cutterWrap);
};

/**
 * When a range is changed
 */
Mdl.prototype.onChangeCrop = function(data) {
  this.vdo.currentTime(data.from);
  this.getModel().handleRange(data.from, data.to);
};

Mdl.prototype.runCutter = function() {
  var duration = this.vdo.duration();
  var wrap = document.createElement('div');

  var movieCrop = mdlMovieCrop.init(wrap, {
    // контекст, если нужен
    scope: null,

    // Кадры из видео, может быть любое кол-во, но оптимально 8-14
    frames: this.getModel().getFrameUrls(),

    // информация о размере видео (в секундах)
    full_length: duration,
    crop_length: this.getModel().crop_length,
    start_from: 0,

    change: this.onChangeCrop.bind(this)
  });
  movieCrop.init();

  this.cutterWrap.appendChild(wrap);
};

/**
 * After metadata is ready
 */
Mdl.prototype.handleMetaReady = function() {
  this.runCutter();
};

/**
 * Generate a view
 */
Mdl.prototype.gnrt = function() {
  var videoElem = document.createElement('video');
  var videoSource = document.createElement('source');
  // get first media_file only
  var mediaFile = this.getModel().arr_media_file[0];
  videoSource.src = mediaFile.url;
  videoSource.type = mediaFile.mime_type;
  videoElem.className = videoElem.className + ' video-js vjs-default-skin';
  videoElem.appendChild(videoSource);
  var playerWrap = document.createElement('div');
  playerWrap.appendChild(videoElem);
  this.block.appendChild(playerWrap);


  var ths = this;
  // Player builds using videojs and inserted a link
  vjsHelper(videoElem, {
    width: '100%',
    height: '100%',
    controls: true,
    preload: true
  }, function() {
    // this = video elem created by video js
    ths.vdo = this;
    this.one('loadedmetadata', ths.handleMetaReady.bind(ths));
  });
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;
