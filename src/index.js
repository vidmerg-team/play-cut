/**
 * Main controller
 *    Dependency: video-js.css with fonts, movie-crop.css with images
 */

var bldMediaSpec = require('./models/media-spec');
var bldVwmMediaSpec = require('./viewmodels/media-spec');

exports.init = function(sources, frames, cropLength, handleRange) {
  var mediaSpec = bldMediaSpec.init({
    arr_media_file: sources,
    arr_media_frame: frames,
    crop_length: cropLength,
    handleRange: handleRange
  });

  var vwmMediaSpec = bldVwmMediaSpec.init(mediaSpec);
  vwmMediaSpec.gnrt();
  return vwmMediaSpec.block;
};

module.exports = exports;
