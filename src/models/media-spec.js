/**
 * Media spec
 * @module
 */

var mdlMediaFile = require('./media-file');
var mdlMediaFrame = require('./media-frame');

/**
 * Media spec
 * @constructor
 */
var Mdl = function(data) {
  Object.keys(data).forEach(this.buildProp.bind(this, data));

  /**
   * Length to crop, seconds (double)
   * @type {Number}
   */
  this.crop_length = data.crop_length;

  /**
   * Callback, returns (start, stop)
   * @type {Function}
   */
  this.handleRange = data.handleRange;

  /** 
   * Media files with url and type of a file
   * @type {Array}
   */
  this.arr_media_file = data.arr_media_file.map(this.buildMediaFile.bind(this));

  /**
   * Frames for a video: images
   * @type {Array}
   */
  this.arr_media_frame = data.arr_media_frame.map(this.buildMediaFrame.bind(this));
};

/**
 * Build a prop for a model
 */
Mdl.prototype.buildProp = function(data, propKey) {
  this[propKey] = data[propKey];
};

/**
 * Init a media file
 */
Mdl.prototype.buildMediaFile = function(item) {
  return mdlMediaFile.init(item);
};

/**
 * Init a media frame
 */
Mdl.prototype.buildMediaFrame = function(item) {
  return mdlMediaFrame.init(item);
};

/**
 * Get array of urls of frames
 * @returns {Array}
 */
Mdl.prototype.getFrameUrls = function() {
  return this.arr_media_frame.map(function(mediaFrame) {
    return mediaFrame.url;
  });
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;
