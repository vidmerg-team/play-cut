/**
 * Gulp tasks
 */

var notify = require('gulp-notify');
var gulpExec = require('gulp-exec');
var gulp = require('gulp');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var path = require('path');
var express = require('express');

var helperPath = path.resolve('task-helpers');

var ntfHelper = require(path.join(helperPath, 'jshint-notify'));

var pth = {};
pth.src = path.resolve('src');
pth.dst = path.resolve('dst');
pth.blibs = path.resolve('node_modules');
pth.demo = path.resolve('demo');

var cbkNotifyOnError = function(err) {
  return err.message;
};

gulp.task('jshint', function() {
  return gulp.src(['./*.js', pth.src + '**/*.js'])
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter(stylish))
    .pipe(notify(ntfHelper.run));
});


var runBwf = function(srcFile, bundleFile) {
  var command = 'browserify <%= file.path %> -o ' + bundleFile;
  //    ' --exclude ' + pth.libs.modernizr +
  //    ' --exclude ' + pth.libs.jquery;

  return gulp.src(srcFile)
    .pipe(gulpExec(command))
    .on('error', notify.onError(cbkNotifyOnError));
};

gulp.task('bwf', ['jshint'], function() {
  return runBwf(path.join(pth.src, 'index.js'), path.join(pth.dst, 'index.js'));
});

gulp.task('copy_crop_img', function() {
  return gulp.src(path.join(pth.blibs, 'movie-crop-slider', 'img', '*.*'))
    .pipe(gulp.dest(path.join(pth.dst, 'libs', 'movie-crop-slider', 'img')));
});

gulp.task('copy_crop_css', function() {
  return gulp.src(path.join(pth.blibs, 'movie-crop-slider', 'css', '*.css'))
    .pipe(gulp.dest(path.join(pth.dst, 'libs', 'movie-crop-slider', 'css')));
});

gulp.task('copy_vjs', function() {
  return gulp.src(path.join(pth.blibs, 'video.js', 'dist', 'video-js', '**/*'))
    .pipe(gulp.dest(path.join(pth.dst, 'libs', 'video-js')));
});

gulp.task('copy_jquery', function() {
  return gulp.src(path.join(pth.blibs, 'jquery', 'dist', '**/*'))
    .pipe(gulp.dest(path.join(pth.dst, 'libs', 'jquery')));
});

gulp.task('build', ['copy_crop_css', 'copy_crop_img', 'copy_vjs', 'copy_jquery']);

gulp.task('demo', ['jshint'], function() {
  return runBwf(path.join(pth.demo, 'demo.js'), path.join(pth.demo, 'demo-bundle.js'));
});

function startExpress() {
  var app = express();
  app.use(express.static('./'));
  app.listen(3321);
  console.log('http://localhost:3321');
}

gulp.task('connect', function() {
  startExpress();
});
