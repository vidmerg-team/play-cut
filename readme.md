play-cut module
===

Steps
---
- input data: 
    - url of a video
    - frames (images)
- init a module (return view and logic)
- load a video
- load frames
- show cutter with frames
- cut the video
- return a cut range

Dependenies
---

- cutter
- videojs


Using
---

```var playCutElem = playCut.init(sources, frames, cropLength, handleRange);```

- sources: Array of video [{ url: 'asdf', mime_type: 'video/mp4'}, ... ]
- frames: Array of media frames: [{ url: 'asdf' }, ...]
- cropLength: length to crop, in seconds (double)
- handleRange = function(start, stop)...


Models
---

Model: media_spec
- array: media_file (input)
- array: media_frame (input)
- crop_length
- handleRange
- duration (dynamically received from arr of media_file)
- item: cut_range (output)

Model: media_file
- url_of_file
- id_of_media_spec (fk)

Model: media_frame
- url_of_frame
- id_of_media_spec (fk)

ViewModel: media_spec_vwm
- mdl: media_spec


Build
---

- copy all libs to dst: gulp build
- create demo: gulp demo