var playCut = require('../index');

var elemPlayCut = document.getElementById('play-cut');
var sources = [{
  url: 'https://s3-eu-west-1.amazonaws.com/vmg-eu-west-1-pre/739459176.mp4',
  mime_type: 'video/mp4'
}];

var loadFrames = function() {
  return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14].map(function(item) {
    return {
      url: 'https://s3-eu-west-1.amazonaws.com/vmg-dev-frame/165709257/000' + ('0' + item).slice(-2) + '.png'
    };
    //    return 'https://s3-eu-west-1.amazonaws.com/vmg-dev-frame/792368719/0000' + item + '.png';
  });
};

var parentWrap = playCut.init(sources, loadFrames(), 25.5, function(start, stop) {
  console.log('from ' + start + ' to ' + stop);
});
elemPlayCut.appendChild(parentWrap);
