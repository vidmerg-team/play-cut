(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var playCut = require('../index');

var elemPlayCut = document.getElementById('play-cut');
var sources = [{
  url: 'https://s3-eu-west-1.amazonaws.com/vmg-eu-west-1-pre/739459176.mp4',
  mime_type: 'video/mp4'
}];

var loadFrames = function() {
  return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14].map(function(item) {
    return {
      url: 'https://s3-eu-west-1.amazonaws.com/vmg-dev-frame/165709257/000' + ('0' + item).slice(-2) + '.png'
    };
    //    return 'https://s3-eu-west-1.amazonaws.com/vmg-dev-frame/792368719/0000' + item + '.png';
  });
};

var parentWrap = playCut.init(sources, loadFrames(), 25.5, function(start, stop) {
  console.log('from ' + start + ' to ' + stop);
});
elemPlayCut.appendChild(parentWrap);

},{"../index":2}],2:[function(require,module,exports){
module.exports = require('./src/index');

},{"./src/index":6}],3:[function(require,module,exports){
// Ion.RangeSlider
// version 2.0.2 Build: 287
// © Denis Ineshin, 2014    https://github.com/IonDen
//
// Project page:    http://ionden.com/a/plugins/ion.rangeSlider/en.html
// GitHub page:     https://github.com/IonDen/ion.rangeSlider
//
// Released under MIT licence:
// http://ionden.com/a/plugins/licence-en.html
// =====================================================================================================================

;(function ($, document, window, navigator, undefined) {
    "use strict";

    // =================================================================================================================
    // Service

    var plugin_count = 0;

    var is_old_ie = (function () {
        var n = navigator.userAgent,
            r = /msie\s\d+/i,
            v;
        if (n.search(r) > 0) {
            v = r.exec(n).toString();
            v = v.split(" ")[1];
            if (v < 9) {
                $("html").addClass("lt-ie9");
                return true;
            }
        }
        return false;
    } ());

    var is_touch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));


    // IE8 fix
    if (!Function.prototype.bind) {
        Function.prototype.bind = function bind(that) {

            var target = this;
            var slice = [].slice;

            if (typeof target != "function") {
                throw new TypeError();
            }

            var args = slice.call(arguments, 1),
                bound = function () {

                    if (this instanceof bound) {

                        var F = function(){};
                        F.prototype = target.prototype;
                        var self = new F();

                        var result = target.apply(
                            self,
                            args.concat(slice.call(arguments))
                        );
                        if (Object(result) === result) {
                            return result;
                        }
                        return self;

                    } else {

                        return target.apply(
                            that,
                            args.concat(slice.call(arguments))
                        );

                    }

                };

            return bound;
        };
    }



    // =================================================================================================================
    // Template

    var base_html =
        '<span class="irs">' +
        '<span class="irs-line" tabindex="-1"><span class="irs-line-left"></span><span class="irs-line-mid"></span><span class="irs-line-right"></span></span>' +
        '<span class="irs-min">0</span><span class="irs-max">1</span>' +
        '<span class="irs-from">0</span><span class="irs-to">0</span><span class="irs-single">0</span>' +
        '</span>' +
        '<span class="irs-grid"></span>' +
        '<span class="irs-bar"></span>';

    var single_html =
        '<span class="irs-bar-edge"></span>' +
        '<span class="irs-shadow shadow-single"></span>' +
        '<span class="irs-slider single"></span>';

    var double_html =
        '<span class="irs-shadow shadow-from"></span>' +
        '<span class="irs-shadow shadow-to"></span>' +
        '<span class="irs-slider from"></span>' +
        '<span class="irs-slider to"></span>';

    var disable_html =
        '<span class="irs-disable-mask"></span>';



    // =================================================================================================================
    // Core

    var IonRangeSlider = function (input, options, plugin_count) {
        this.VERSION = "2.0.2";
        this.input = input;
        this.plugin_count = plugin_count;
        this.current_plugin = 0;
        this.calc_count = 0;
        this.old_from = 0;
        this.old_to = 0;
        this.raf_id = null;
        this.dragging = false;
        this.force_redraw = false;
        this.is_key = false;
        this.is_update = false;
        this.is_start = true;
        this.is_active = false;
        this.is_resize = false;
        this.is_click = false;

        this.$cache = {
            win: $(window),
            body: $(document.body),
            input: $(input),
            cont: null,
            rs: null,
            min: null,
            max: null,
            from: null,
            to: null,
            single: null,
            bar: null,
            line: null,
            s_single: null,
            s_from: null,
            s_to: null,
            shad_single: null,
            shad_from: null,
            shad_to: null,
            grid: null,
            grid_labels: []
        };

        // get config data attributes
        var $inp = this.$cache.input;
        var data = {
            type: $inp.data("type"),

            min: $inp.data("min"),
            max: $inp.data("max"),
            from: $inp.data("from"),
            to: $inp.data("to"),
            step: $inp.data("step"),

            min_interval: $inp.data("minInterval"),
            max_interval: $inp.data("maxInterval"),
            drag_interval: $inp.data("dragInterval"),

            values: $inp.data("values"),

            from_fixed: $inp.data("fromFixed"),
            from_min: $inp.data("fromMin"),
            from_max: $inp.data("fromMax"),
            from_shadow: $inp.data("fromShadow"),

            to_fixed: $inp.data("toFixed"),
            to_min: $inp.data("toMin"),
            to_max: $inp.data("toMax"),
            to_shadow: $inp.data("toShadow"),

            prettify_enabled: $inp.data("prettifyEnabled"),
            prettify_separator: $inp.data("prettifySeparator"),

            force_edges: $inp.data("forceEdges"),

            keyboard: $inp.data("keyboard"),
            keyboard_step: $inp.data("keyboardStep"),

            grid: $inp.data("grid"),
            grid_margin: $inp.data("gridMargin"),
            grid_num: $inp.data("gridNum"),
            grid_snap: $inp.data("gridSnap"),

            hide_min_max: $inp.data("hideMinMax"),
            hide_from_to: $inp.data("hideFromTo"),

            prefix: $inp.data("prefix"),
            postfix: $inp.data("postfix"),
            max_postfix: $inp.data("maxPostfix"),
            decorate_both: $inp.data("decorateBoth"),
            values_separator: $inp.data("valuesSeparator"),

            disable: $inp.data("disable")
        };
        data.values = data.values && data.values.split(",");
        options = $.extend(data, options);

        // get config from options
        this.options = $.extend({
            type: "single",

            min: 10,
            max: 100,
            from: null,
            to: null,
            step: 1,

            min_interval: 0,
            max_interval: 0,
            drag_interval: false,

            values: [],
            p_values: [],

            from_fixed: false,
            from_min: null,
            from_max: null,
            from_shadow: false,

            to_fixed: false,
            to_min: null,
            to_max: null,
            to_shadow: false,

            prettify_enabled: true,
            prettify_separator: " ",
            prettify: null,

            force_edges: false,

            keyboard: false,
            keyboard_step: 5,

            grid: false,
            grid_margin: true,
            grid_num: 4,
            grid_snap: false,

            hide_min_max: false,
            hide_from_to: false,

            prefix: "",
            postfix: "",
            max_postfix: "",
            decorate_both: true,
            values_separator: " — ",

            disable: false,

            onStart: null,
            onChange: null,
            onFinish: null,
            onUpdate: null
        }, options);

        this.validate();

        this.result = {
            input: this.$cache.input,
            slider: null,

            min: this.options.min,
            max: this.options.max,

            from: this.options.from,
            from_percent: 0,
            from_value: null,

            to: this.options.to,
            to_percent: 0,
            to_value: null
        };

        this.coords = {
            // left
            x_gap: 0,
            x_pointer: 0,

            // width
            w_rs: 0,
            w_rs_old: 0,
            w_handle: 0,

            // percents
            p_gap: 0,
            p_gap_left: 0,
            p_gap_right: 0,
            p_step: 0,
            p_pointer: 0,
            p_handle: 0,
            p_single: 0,
            p_single_real: 0,
            p_from: 0,
            p_from_real: 0,
            p_to: 0,
            p_to_real: 0,
            p_bar_x: 0,
            p_bar_w: 0,

            // grid
            grid_gap: 0,
            big_num: 0,
            big: [],
            big_w: [],
            big_p: [],
            big_x: []
        };

        this.labels = {
            // width
            w_min: 0,
            w_max: 0,
            w_from: 0,
            w_to: 0,
            w_single: 0,

            // percents
            p_min: 0,
            p_max: 0,
            p_from: 0,
            p_from_left: 0,
            p_to: 0,
            p_to_left: 0,
            p_single: 0,
            p_single_left: 0
        };

        this.init();
    };

    IonRangeSlider.prototype = {
        init: function (is_update) {
            this.coords.p_step = this.options.step / ((this.options.max - this.options.min) / 100);
            this.target = "base";

            this.toggleInput();
            this.append();
            this.setMinMax();

            if (is_update) {
                this.force_redraw = true;
                this.calc(true);

                if (this.options.onUpdate && typeof this.options.onUpdate === "function") {
                    this.options.onUpdate(this.result);
                }
            } else {
                this.force_redraw = true;
                this.calc(true);

                if (this.options.onStart && typeof this.options.onStart === "function") {
                    this.options.onStart(this.result);
                }
            }

            this.updateScene();
            this.raf_id = requestAnimationFrame(this.updateScene.bind(this));
        },

        append: function () {
            var container_html = '<span class="irs js-irs-' + this.plugin_count + '"></span>';
            this.$cache.input.before(container_html);
            this.$cache.input.prop("readonly", true);
            this.$cache.cont = this.$cache.input.prev();
            this.result.slider = this.$cache.cont;

            this.$cache.cont.html(base_html);
            this.$cache.rs = this.$cache.cont.find(".irs");
            this.$cache.min = this.$cache.cont.find(".irs-min");
            this.$cache.max = this.$cache.cont.find(".irs-max");
            this.$cache.from = this.$cache.cont.find(".irs-from");
            this.$cache.to = this.$cache.cont.find(".irs-to");
            this.$cache.single = this.$cache.cont.find(".irs-single");
            this.$cache.bar = this.$cache.cont.find(".irs-bar");
            this.$cache.line = this.$cache.cont.find(".irs-line");
            this.$cache.grid = this.$cache.cont.find(".irs-grid");

            if (this.options.type === "single") {
                this.$cache.cont.append(single_html);
                this.$cache.s_single = this.$cache.cont.find(".single");
                this.$cache.from[0].style.visibility = "hidden";
                this.$cache.to[0].style.visibility = "hidden";
                this.$cache.shad_single = this.$cache.cont.find(".shadow-single");
            } else {
                this.$cache.cont.append(double_html);
                this.$cache.s_from = this.$cache.cont.find(".from");
                this.$cache.s_to = this.$cache.cont.find(".to");
                this.$cache.shad_from = this.$cache.cont.find(".shadow-from");
                this.$cache.shad_to = this.$cache.cont.find(".shadow-to");
            }

            if (this.options.hide_from_to) {
                this.$cache.from[0].style.display = "none";
                this.$cache.to[0].style.display = "none";
                this.$cache.single[0].style.display = "none";
            }

            this.appendGrid();

            if (this.options.disable) {
                this.appendDisableMask();
            } else {
                this.$cache.cont.removeClass("irs-disabled");
                this.bindEvents();
            }
        },

        appendDisableMask: function () {
            this.$cache.cont.append(disable_html);
            this.$cache.cont.addClass("irs-disabled");
        },

        remove: function () {
            this.$cache.cont.remove();
            this.$cache.cont = null;

            this.$cache.line.off("keydown.irs_" + this.plugin_count);

            if (is_touch) {

                this.$cache.body.off("touchmove.irs_" + this.plugin_count);
                this.$cache.win.off("touchend.irs_" + this.plugin_count);

            } else {

                this.$cache.body.off("mousemove.irs_" + this.plugin_count);
                this.$cache.win.off("mouseup.irs_" + this.plugin_count);

                if (is_old_ie) {
                    this.$cache.body.off("mouseup.irs_" + this.plugin_count);
                    this.$cache.body.off("mouseleave.irs_" + this.plugin_count);
                }

            }

            this.$cache.grid_labels = [];
            this.coords.big = [];
            this.coords.big_w = [];
            this.coords.big_p = [];
            this.coords.big_x = [];

            cancelAnimationFrame(this.raf_id);
        },

        bindEvents: function () {
            if (is_touch) {

                this.$cache.body.on("touchmove.irs_" + this.plugin_count, this.pointerMove.bind(this));
                this.$cache.win.on("touchend.irs_" + this.plugin_count, this.pointerUp.bind(this));

                this.$cache.line.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));

                if (this.options.drag_interval && this.options.type === "double") {
                    this.$cache.bar.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "both"));
                } else {
                    this.$cache.bar.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                }

                if (this.options.type === "single") {
                    this.$cache.s_single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
                    this.$cache.shad_single.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                } else {
                    this.$cache.s_from.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
                    this.$cache.s_to.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
                    this.$cache.shad_from.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                    this.$cache.shad_to.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                }

            } else {

                if (this.options.keyboard) {
                    this.$cache.line.on("keydown.irs_" + this.plugin_count, this.key.bind(this, "keyboard"));
                }

                this.$cache.body.on("mousemove.irs_" + this.plugin_count, this.pointerMove.bind(this));
                this.$cache.win.on("mouseup.irs_" + this.plugin_count, this.pointerUp.bind(this));

                if (is_old_ie) {
                    this.$cache.body.on("mouseup.irs_" + this.plugin_count, this.pointerUp.bind(this));
                    this.$cache.body.on("mouseleave.irs_" + this.plugin_count, this.pointerUp.bind(this));
                }

                this.$cache.line.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));

                if (this.options.drag_interval && this.options.type === "double") {
                    this.$cache.bar.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "both"));
                } else {
                    this.$cache.bar.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                }


                if (this.options.type === "single") {
                    this.$cache.s_single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
                    this.$cache.shad_single.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                } else {
                    this.$cache.s_from.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
                    this.$cache.s_to.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
                    this.$cache.shad_from.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                    this.$cache.shad_to.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                }

            }
        },

        pointerMove: function (e) {
            if (!this.dragging) {
                return;
            }
            var e_base = is_touch ? e.originalEvent.touches[0] : e;
            this.coords.x_pointer = e_base.pageX - this.coords.x_gap;

            this.calc();
        },

        pointerUp: function (e) {
            if (this.current_plugin !== this.plugin_count) {
                return;
            }

            if (this.is_active) {
                this.is_active = false;
            } else {
                return;
            }

            var is_function = this.options.onFinish && typeof this.options.onFinish === "function",
                is_original = $.contains(this.$cache.cont[0], e.target) || this.dragging;

            if (is_function && is_original) {
                this.options.onFinish(this.result);
            }

            this.force_redraw = true;
            this.dragging = false;

            if (is_old_ie) {
                $("*").prop("unselectable", false);
            }
        },

        pointerDown: function (target, e) {
            e.preventDefault();
            var e_base = is_touch ? e.originalEvent.touches[0] : e;
            if (e.button === 2) {
                return;
            }

            this.current_plugin = this.plugin_count;
            this.target = target;

            this.is_active = true;
            this.dragging = true;

            this.coords.x_gap = this.$cache.rs.offset().left;
            this.coords.x_pointer = e_base.pageX - this.coords.x_gap;

            this.calcPointer();

            switch (target) {
                case "single":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_single);
                    break;
                case "from":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_from);
                    this.$cache.s_from.addClass("type_last");
                    this.$cache.s_to.removeClass("type_last");
                    break;
                case "to":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_to);
                    this.$cache.s_to.addClass("type_last");
                    this.$cache.s_from.removeClass("type_last");
                    break;
                case "both":
                    this.coords.p_gap_left = this.toFixed(this.coords.p_pointer - this.coords.p_from);
                    this.coords.p_gap_right = this.toFixed(this.coords.p_to - this.coords.p_pointer);
                    this.$cache.s_to.removeClass("type_last");
                    this.$cache.s_from.removeClass("type_last");
                    break;
            }

            if (is_old_ie) {
                $("*").prop("unselectable", true);
            }

            this.$cache.line.trigger("focus");
        },

        pointerClick: function (target, e) {
            e.preventDefault();
            var e_base = is_touch ? e.originalEvent.touches[0] : e;
            if (e.button === 2) {
                return;
            }

            this.current_plugin = this.plugin_count;
            this.target = target;

            this.is_click = true;
            this.coords.x_gap = this.$cache.rs.offset().left;
            this.coords.x_pointer = +(e_base.pageX - this.coords.x_gap).toFixed();

            this.force_redraw = true;
            this.calc();

            this.$cache.line.trigger("focus");
        },

        key: function (target, e) {
            if (this.current_plugin !== this.plugin_count || e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
                return;
            }

            switch (e.which) {
                case 83: // W
                case 65: // A
                case 40: // DOWN
                case 37: // LEFT
                    e.preventDefault();
                    this.moveByKey(false);
                    break;

                case 87: // S
                case 68: // D
                case 38: // UP
                case 39: // RIGHT
                    e.preventDefault();
                    this.moveByKey(true);
                    break;
            }

            return true;
        },

        // Move by key beta
        // TODO: refactor than have plenty of time
        moveByKey: function (right) {
            var p = this.coords.p_pointer;

            if (right) {
                p += this.options.keyboard_step;
            } else {
                p -= this.options.keyboard_step;
            }

            this.coords.x_pointer = this.toFixed(this.coords.w_rs / 100 * p);
            this.is_key = true;
            this.calc();
        },

        setMinMax: function () {
            if (this.options.hide_min_max) {
                this.$cache.min[0].style.display = "none";
                this.$cache.max[0].style.display = "none";
                return;
            }

            if (this.options.values.length) {
                this.$cache.min.html(this.decorate(this.options.p_values[this.options.min]));
                this.$cache.max.html(this.decorate(this.options.p_values[this.options.max]));
            } else {
                this.$cache.min.html(this.decorate(this._prettify(this.options.min), this.options.min));
                this.$cache.max.html(this.decorate(this._prettify(this.options.max), this.options.max));
            }

            this.labels.w_min = this.$cache.min.outerWidth(false);
            this.labels.w_max = this.$cache.max.outerWidth(false);
        },



        // =============================================================================================================
        // Calculations

        calc: function (update) {
            this.calc_count++;

            if (this.calc_count === 10 || update) {
                this.calc_count = 0;
                this.coords.w_rs = this.$cache.rs.outerWidth(false);
                if (this.options.type === "single") {
                    this.coords.w_handle = this.$cache.s_single.outerWidth(false);
                } else {
                    this.coords.w_handle = this.$cache.s_from.outerWidth(false);
                }
            }

            if (!this.coords.w_rs) {
                return;
            }

            this.calcPointer();

            this.coords.p_handle = this.toFixed(this.coords.w_handle / this.coords.w_rs * 100);
            var real_width = 100 - this.coords.p_handle,
                real_x = this.toFixed(this.coords.p_pointer - this.coords.p_gap);

            if (this.target === "click") {
                real_x = this.toFixed(this.coords.p_pointer - (this.coords.p_handle / 2));
                this.target = this.chooseHandle(real_x);
            }

            if (real_x < 0) {
                real_x = 0;
            } else if (real_x > real_width) {
                real_x = real_width;
            }

            switch (this.target) {
                case "base":
                    var w = (this.options.max - this.options.min) / 100,
                        f = (this.result.from - this.options.min) / w,
                        t = (this.result.to - this.options.min) / w;

                    this.coords.p_single_real = this.toFixed(f);
                    this.coords.p_from_real = this.toFixed(f);
                    this.coords.p_to_real = this.toFixed(t);

                    this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);

                    this.coords.p_single = this.toFixed(f - (this.coords.p_handle / 100 * f));
                    this.coords.p_from = this.toFixed(f - (this.coords.p_handle / 100 * f));
                    this.coords.p_to = this.toFixed(t - (this.coords.p_handle / 100 * t));

                    this.target = null;

                    break;

                case "single":
                    if (this.options.from_fixed) {
                        break;
                    }

                    this.coords.p_single_real = this.calcWithStep(real_x / real_width * 100);
                    this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max);
                    this.coords.p_single = this.toFixed(this.coords.p_single_real / 100 * real_width);

                    break;

                case "from":
                    if (this.options.from_fixed) {
                        break;
                    }

                    this.coords.p_from_real = this.calcWithStep(real_x / real_width * 100);
                    if (this.coords.p_from_real > this.coords.p_to_real) {
                        this.coords.p_from_real = this.coords.p_to_real;
                    }
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, "from");
                    this.coords.p_from_real = this.checkMaxInterval(this.coords.p_from_real, this.coords.p_to_real, "from");
                    this.coords.p_from = this.toFixed(this.coords.p_from_real / 100 * real_width);

                    break;

                case "to":
                    if (this.options.to_fixed) {
                        break;
                    }

                    this.coords.p_to_real = this.calcWithStep(real_x / real_width * 100);
                    if (this.coords.p_to_real < this.coords.p_from_real) {
                        this.coords.p_to_real = this.coords.p_from_real;
                    }
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, "to");
                    this.coords.p_to_real = this.checkMaxInterval(this.coords.p_to_real, this.coords.p_from_real, "to");
                    this.coords.p_to = this.toFixed(this.coords.p_to_real / 100 * real_width);

                    break;

                case "both":
                    real_x = this.toFixed(real_x + (this.coords.p_handle * 0.1));

                    this.coords.p_from_real = this.calcWithStep((real_x - this.coords.p_gap_left) / real_width * 100);
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, "from");
                    this.coords.p_from = this.toFixed(this.coords.p_from_real / 100 * real_width);

                    this.coords.p_to_real = this.calcWithStep((real_x + this.coords.p_gap_right) / real_width * 100);
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, "to");
                    this.coords.p_to = this.toFixed(this.coords.p_to_real / 100 * real_width);

                    break;
            }

            if (this.options.type === "single") {
                this.coords.p_bar_x = (this.coords.p_handle / 2);
                this.coords.p_bar_w = this.coords.p_single;

                this.result.from_percent = this.coords.p_single_real;
                this.result.from = this.calcReal(this.coords.p_single_real);
                if (this.options.values.length) {
                    this.result.from_value = this.options.values[this.result.from];
                }
            } else {
                this.coords.p_bar_x = this.toFixed(this.coords.p_from + (this.coords.p_handle / 2));
                this.coords.p_bar_w = this.toFixed(this.coords.p_to - this.coords.p_from);

                this.result.from_percent = this.coords.p_from_real;
                this.result.from = this.calcReal(this.coords.p_from_real);
                this.result.to_percent = this.coords.p_to_real;
                this.result.to = this.calcReal(this.coords.p_to_real);
                if (this.options.values.length) {
                    this.result.from_value = this.options.values[this.result.from];
                    this.result.to_value = this.options.values[this.result.to];
                }
            }

            this.calcMinMax();
            this.calcLabels();
        },

        calcPointer: function () {
            if (!this.coords.w_rs) {
                this.coords.p_pointer = 0;
                return;
            }

            if (this.coords.x_pointer < 0) {
                this.coords.x_pointer = 0;
            } else if (this.coords.x_pointer > this.coords.w_rs) {
                this.coords.x_pointer = this.coords.w_rs;
            }

            this.coords.p_pointer = this.toFixed(this.coords.x_pointer / this.coords.w_rs * 100);
        },

        chooseHandle: function (real_x) {
            if (this.options.type === "single") {
                return "single";
            } else {
                var m_point = this.coords.p_from_real + ((this.coords.p_to_real - this.coords.p_from_real) / 2);
                if (real_x >= m_point) {
                    return "to";
                } else {
                    return "from";
                }
            }
        },

        calcMinMax: function () {
            if (!this.coords.w_rs) {
                return;
            }

            this.labels.p_min = this.labels.w_min / this.coords.w_rs * 100;
            this.labels.p_max = this.labels.w_max / this.coords.w_rs * 100;
        },

        calcLabels: function () {
            if (!this.coords.w_rs || this.options.hide_from_to) {
                return;
            }

            if (this.options.type === "single") {

                this.labels.w_single = this.$cache.single.outerWidth(false);
                this.labels.p_single = this.labels.w_single / this.coords.w_rs * 100;
                this.labels.p_single_left = this.coords.p_single + (this.coords.p_handle / 2) - (this.labels.p_single / 2);
                this.labels.p_single_left = this.checkEdges(this.labels.p_single_left, this.labels.p_single);

            } else {

                this.labels.w_from = this.$cache.from.outerWidth(false);
                this.labels.p_from = this.labels.w_from / this.coords.w_rs * 100;
                this.labels.p_from_left = this.coords.p_from + (this.coords.p_handle / 2) - (this.labels.p_from / 2);
                this.labels.p_from_left = this.toFixed(this.labels.p_from_left);
                this.labels.p_from_left = this.checkEdges(this.labels.p_from_left, this.labels.p_from);

                this.labels.w_to = this.$cache.to.outerWidth(false);
                this.labels.p_to = this.labels.w_to / this.coords.w_rs * 100;
                this.labels.p_to_left = this.coords.p_to + (this.coords.p_handle / 2) - (this.labels.p_to / 2);
                this.labels.p_to_left = this.toFixed(this.labels.p_to_left);
                this.labels.p_to_left = this.checkEdges(this.labels.p_to_left, this.labels.p_to);

                this.labels.w_single = this.$cache.single.outerWidth(false);
                this.labels.p_single = this.labels.w_single / this.coords.w_rs * 100;
                this.labels.p_single_left = ((this.labels.p_from_left + this.labels.p_to_left + this.labels.p_to) / 2) - (this.labels.p_single / 2);
                this.labels.p_single_left = this.toFixed(this.labels.p_single_left);
                this.labels.p_single_left = this.checkEdges(this.labels.p_single_left, this.labels.p_single);

            }
        },



        // =============================================================================================================
        // Drawings

        updateScene: function () {
            this.drawHandles();

            this.raf_id = requestAnimationFrame(this.updateScene.bind(this));
        },

        drawHandles: function () {
            this.coords.w_rs = this.$cache.rs.outerWidth(false);

            if (this.coords.w_rs !== this.coords.w_rs_old) {
                this.target = "base";
                this.is_resize = true;
            }

            if (this.coords.w_rs !== this.coords.w_rs_old || this.force_redraw) {
                this.setMinMax();
                this.calc(true);
                this.drawLabels();
                if (this.options.grid) {
                    this.calcGridMargin();
                    this.calcGridLabels();
                }
                this.force_redraw = true;
                this.coords.w_rs_old = this.coords.w_rs;
                this.drawShadow();
            }

            if (!this.coords.w_rs) {
                return;
            }

            if (!this.dragging && !this.force_redraw && !this.is_key) {
                return;
            }

            if (this.old_from !== this.result.from || this.old_to !== this.result.to || this.force_redraw || this.is_key) {

                this.drawLabels();

                this.$cache.bar[0].style.left = this.coords.p_bar_x + "%";
                this.$cache.bar[0].style.width = this.coords.p_bar_w + "%";

                if (this.options.type === "single") {
                    this.$cache.s_single[0].style.left = this.coords.p_single + "%";

                    this.$cache.single[0].style.left = this.labels.p_single_left + "%";

                    if (this.options.values.length) {
                        this.$cache.input.prop("value", this.result.from_value);
                        this.$cache.input.data("from", this.result.from_value);
                    } else {
                        this.$cache.input.prop("value", this.result.from);
                        this.$cache.input.data("from", this.result.from);
                    }
                } else {
                    this.$cache.s_from[0].style.left = this.coords.p_from + "%";
                    this.$cache.s_to[0].style.left = this.coords.p_to + "%";

                    if (this.old_from !== this.result.from || this.force_redraw) {
                        this.$cache.from[0].style.left = this.labels.p_from_left + "%";
                    }
                    if (this.old_to !== this.result.to || this.force_redraw) {
                        this.$cache.to[0].style.left = this.labels.p_to_left + "%";
                    }

                    this.$cache.single[0].style.left = this.labels.p_single_left + "%";

                    if (this.options.values.length) {
                        this.$cache.input.prop("value", this.result.from_value + ";" + this.result.to_value);
                        this.$cache.input.data("from", this.result.from_value);
                        this.$cache.input.data("to", this.result.to_value);
                    } else {
                        this.$cache.input.prop("value", this.result.from + ";" + this.result.to);
                        this.$cache.input.data("from", this.result.from);
                        this.$cache.input.data("to", this.result.to);
                    }
                }

                if ((this.old_from !== this.result.from || this.old_to !== this.result.to) && !this.is_start) {
                    this.$cache.input.trigger("change");
                }

                this.old_from = this.result.from;
                this.old_to = this.result.to;

                var is_function = this.options.onChange && typeof this.options.onChange === "function" && !this.is_resize;
                if (is_function && !this.is_update && !this.is_start) {
                    this.options.onChange(this.result);
                }

                var is_finish = this.options.onFinish && typeof this.options.onFinish === "function";
                if (is_finish && (this.is_key || this.is_click)) {
                    this.options.onFinish(this.result);
                }

                this.is_update = false;
                this.is_resize = false;
            }

            this.is_start = false;
            this.is_key = false;
            this.is_click = false;
            this.force_redraw = false;
        },

        drawLabels: function () {
            var values_num = this.options.values.length,
                p_values = this.options.p_values,
                text_single,
                text_from,
                text_to;

            if (this.options.hide_from_to) {
                return;
            }

            if (this.options.type === "single") {

                if (values_num) {
                    text_single = this.decorate(p_values[this.result.from]);
                    this.$cache.single.html(text_single);
                } else {
                    text_single = this.decorate(this._prettify(this.result.from), this.result.from);
                    this.$cache.single.html(text_single);
                }

                this.calcLabels();

                if (this.labels.p_single_left < this.labels.p_min + 1) {
                    this.$cache.min[0].style.visibility = "hidden";
                } else {
                    this.$cache.min[0].style.visibility = "visible";
                }

                if (this.labels.p_single_left + this.labels.p_single > 100 - this.labels.p_max - 1) {
                    this.$cache.max[0].style.visibility = "hidden";
                } else {
                    this.$cache.max[0].style.visibility = "visible";
                }

            } else {

                if (values_num) {

                    if (this.options.decorate_both) {
                        text_single = this.decorate(p_values[this.result.from]);
                        text_single += this.options.values_separator;
                        text_single += this.decorate(p_values[this.result.to]);
                    } else {
                        text_single = this.decorate(p_values[this.result.from] + this.options.values_separator + p_values[this.result.to]);
                    }
                    text_from = this.decorate(p_values[this.result.from]);
                    text_to = this.decorate(p_values[this.result.to]);

                    this.$cache.single.html(text_single);
                    this.$cache.from.html(text_from);
                    this.$cache.to.html(text_to);

                } else {

                    if (this.options.decorate_both) {
                        text_single = this.decorate(this._prettify(this.result.from));
                        text_single += this.options.values_separator;
                        text_single += this.decorate(this._prettify(this.result.to));
                    } else {
                        text_single = this.decorate(this._prettify(this.result.from) + this.options.values_separator + this._prettify(this.result.to), this.result.from);
                    }
                    text_from = this.decorate(this._prettify(this.result.from), this.result.from);
                    text_to = this.decorate(this._prettify(this.result.to), this.result.to);

                    this.$cache.single.html(text_single);
                    this.$cache.from.html(text_from);
                    this.$cache.to.html(text_to);

                }

                this.calcLabels();

                var min = Math.min(this.labels.p_single_left, this.labels.p_from_left),
                    single_left = this.labels.p_single_left + this.labels.p_single,
                    to_left = this.labels.p_to_left + this.labels.p_to,
                    max = Math.max(single_left, to_left);

                if (this.labels.p_from_left + this.labels.p_from >= this.labels.p_to_left) {
                    this.$cache.from[0].style.visibility = "hidden";
                    this.$cache.to[0].style.visibility = "hidden";
                    this.$cache.single[0].style.visibility = "visible";

                    if (this.result.from === this.result.to) {
                        this.$cache.from[0].style.visibility = "visible";
                        this.$cache.single[0].style.visibility = "hidden";
                        max = to_left;
                    } else {
                        this.$cache.from[0].style.visibility = "hidden";
                        this.$cache.single[0].style.visibility = "visible";
                        max = Math.max(single_left, to_left);
                    }
                } else {
                    this.$cache.from[0].style.visibility = "visible";
                    this.$cache.to[0].style.visibility = "visible";
                    this.$cache.single[0].style.visibility = "hidden";
                }

                if (min < this.labels.p_min + 1) {
                    this.$cache.min[0].style.visibility = "hidden";
                } else {
                    this.$cache.min[0].style.visibility = "visible";
                }

                if (max > 100 - this.labels.p_max - 1) {
                    this.$cache.max[0].style.visibility = "hidden";
                } else {
                    this.$cache.max[0].style.visibility = "visible";
                }

            }
        },

        drawShadow: function () {
            var o = this.options,
                c = this.$cache,
                from_min,
                from_max,
                to_min,
                to_max;

            if (o.type === "single") {
                if (o.from_shadow && (o.from_min || o.from_max)) {
                    from_min = this.calcPercent(o.from_min || o.min);
                    from_max = this.calcPercent(o.from_max || o.max) - from_min;
                    from_min = this.toFixed(from_min - (this.coords.p_handle / 100 * from_min));
                    from_max = this.toFixed(from_max - (this.coords.p_handle / 100 * from_max));
                    from_min = from_min + (this.coords.p_handle / 2);

                    c.shad_single[0].style.display = "block";
                    c.shad_single[0].style.left = from_min + "%";
                    c.shad_single[0].style.width = from_max + "%";
                } else {
                    c.shad_single[0].style.display = "none";
                }
            } else {
                if (o.from_shadow && (o.from_min || o.from_max)) {
                    from_min = this.calcPercent(o.from_min || o.min);
                    from_max = this.calcPercent(o.from_max || o.max) - from_min;
                    from_min = this.toFixed(from_min - (this.coords.p_handle / 100 * from_min));
                    from_max = this.toFixed(from_max - (this.coords.p_handle / 100 * from_max));
                    from_min = from_min + (this.coords.p_handle / 2);

                    c.shad_from[0].style.display = "block";
                    c.shad_from[0].style.left = from_min + "%";
                    c.shad_from[0].style.width = from_max + "%";
                } else {
                    c.shad_from[0].style.display = "none";
                }

                if (o.to_shadow && (o.to_min || o.to_max)) {
                    to_min = this.calcPercent(o.to_min || o.min);
                    to_max = this.calcPercent(o.to_max || o.max) - to_min;
                    to_min = this.toFixed(to_min - (this.coords.p_handle / 100 * to_min));
                    to_max = this.toFixed(to_max - (this.coords.p_handle / 100 * to_max));
                    to_min = to_min + (this.coords.p_handle / 2);

                    c.shad_to[0].style.display = "block";
                    c.shad_to[0].style.left = to_min + "%";
                    c.shad_to[0].style.width = to_max + "%";
                } else {
                    c.shad_to[0].style.display = "none";
                }
            }
        },



        // =============================================================================================================
        // Service methods

        toggleInput: function () {
            this.$cache.input.toggleClass("irs-hidden-input");
        },

        calcPercent: function (num) {
            var w = (this.options.max - this.options.min) / 100,
                percent = (num - this.options.min) / w;

            return this.toFixed(percent);
        },

        calcReal: function (percent) {
            var min = this.options.min,
                max = this.options.max,
                abs = 0;

            if (min < 0) {
                abs = Math.abs(min);
                min = min + abs;
                max = max + abs;
            }

            var number = ((max - min) / 100 * percent) + min,
                string = this.options.step.toString().split(".")[1];

            if (string) {
                number = +number.toFixed(string.length);
            } else {
                number = number / this.options.step;
                number = number * this.options.step;
                number = +number.toFixed(0);
            }

            if (abs) {
                number -= abs;
            }

            if (number < this.options.min) {
                number = this.options.min;
            } else if (number > this.options.max) {
                number = this.options.max;
            }

            if (string) {
                return +number.toFixed(string.length);
            } else {
                return this.toFixed(number);
            }
        },

        calcWithStep: function (percent) {
            var rounded = Math.round(percent / this.coords.p_step) * this.coords.p_step;

            if (rounded > 100) {
                rounded = 100;
            }
            if (percent === 100) {
                rounded = 100;
            }

            return this.toFixed(rounded);
        },

        checkMinInterval: function (p_current, p_next, type) {
            var o = this.options,
                current,
                next;

            if (!o.min_interval) {
                return p_current;
            }

            current = this.calcReal(p_current);
            next = this.calcReal(p_next);

            if (type === "from") {

                if (next - current < o.min_interval) {
                    current = next - o.min_interval;
                }

            } else {

                if (current - next < o.min_interval) {
                    current = next + o.min_interval;
                }

            }

            return this.calcPercent(current);
        },

        checkMaxInterval: function (p_current, p_next, type) {
            var o = this.options,
                current,
                next;

            if (!o.max_interval) {
                return p_current;
            }

            current = this.calcReal(p_current);
            next = this.calcReal(p_next);

            if (type === "from") {

                if (next - current > o.max_interval) {
                    current = next - o.max_interval;
                }

            } else {

                if (current - next > o.max_interval) {
                    current = next + o.max_interval;
                }

            }

            return this.calcPercent(current);
        },

        checkDiapason: function (p_num, min, max) {
            var num = this.calcReal(p_num),
                o = this.options;

            if (!min || typeof min !== "number") {
                min = o.min;
            }

            if (!max || typeof max !== "number") {
                max = o.max;
            }

            if (num < min) {
                num = min;
            }

            if (num > max) {
                num = max;
            }

            return this.calcPercent(num);
        },

        toFixed: function (num) {
            num = num.toFixed(5);
            return +num;
        },

        _prettify: function (num) {
            if (!this.options.prettify_enabled) {
                return num;
            }

            if (this.options.prettify && typeof this.options.prettify === "function") {
                return this.options.prettify(num);
            } else {
                return this.prettify(num);
            }
        },

        prettify: function (num) {
            var n = num.toString();
            return n.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + this.options.prettify_separator);
        },

        checkEdges: function (left, width) {
            if (!this.options.force_edges) {
                return this.toFixed(left);
            }

            if (left < 0) {
                left = 0;
            } else if (left > 100 - width) {
                left = 100 - width;
            }

            return this.toFixed(left);
        },

        validate: function () {
            var o = this.options,
                r = this.result,
                v = o.values,
                vl = v.length,
                value,
                i;

            if (typeof o.min === "string") o.min = +o.min;
            if (typeof o.max === "string") o.max = +o.max;
            if (typeof o.from === "string") o.from = +o.from;
            if (typeof o.to === "string") o.to = +o.to;
            if (typeof o.step === "string") o.step = +o.step;

            if (typeof o.from_min === "string") o.from_min = +o.from_min;
            if (typeof o.from_max === "string") o.from_max = +o.from_max;
            if (typeof o.to_min === "string") o.to_min = +o.to_min;
            if (typeof o.to_max === "string") o.to_max = +o.to_max;

            if (typeof o.keyboard_step === "string") o.keyboard_step = +o.keyboard_step;
            if (typeof o.grid_num === "string") o.grid_num = +o.grid_num;

            if (o.max <= o.min) {
                if (o.min) {
                    o.max = o.min * 2;
                } else {
                    o.max = o.min + 1;
                }
                o.step = 1;
            }

            if (vl) {
                o.p_values = [];
                o.min = 0;
                o.max = vl - 1;
                o.step = 1;
                o.grid_num = o.max;
                o.grid_snap = true;


                for (i = 0; i < vl; i++) {
                    value = +v[i];

                    if (!isNaN(value)) {
                        v[i] = value;
                        value = this._prettify(value);
                    } else {
                        value = v[i];
                    }

                    o.p_values.push(value);
                }
            }

            if (typeof o.from !== "number" || isNaN(o.from)) {
                o.from = o.min;
            }

            if (typeof o.to !== "number" || isNaN(o.from)) {
                o.to = o.max;
            }

            if (o.from < o.min || o.from > o.max) {
                o.from = o.min;
            }

            if (o.to > o.max || o.to < o.min) {
                o.to = o.max;
            }

            if (o.type === "double" && o.from > o.to) {
                o.from = o.to;
            }

            if (typeof o.step !== "number" || isNaN(o.step) || !o.step || o.step < 0) {
                o.step = 1;
            }

            if (typeof o.keyboard_step !== "number" || isNaN(o.keyboard_step) || !o.keyboard_step || o.keyboard_step < 0) {
                o.keyboard_step = 5;
            }

            if (o.from_min && o.from < o.from_min) {
                o.from = o.from_min;
            }

            if (o.from_max && o.from > o.from_max) {
                o.from = o.from_max;
            }

            if (o.to_min && o.to < o.to_min) {
                o.to = o.to_min;
            }

            if (o.to_max && o.from > o.to_max) {
                o.to = o.to_max;
            }

            if (r) {
                if (r.min !== o.min) {
                    r.min = o.min;
                }

                if (r.max !== o.max) {
                    r.max = o.max;
                }

                if (r.from < r.min || r.from > r.max) {
                    r.from = o.from;
                }

                if (r.to < r.min || r.to > r.max) {
                    r.to = o.to;
                }
            }

            if (typeof o.min_interval !== "number" || isNaN(o.min_interval) || !o.min_interval || o.min_interval < 0) {
                o.min_interval = 0;
            }

            if (typeof o.max_interval !== "number" || isNaN(o.max_interval) || !o.max_interval || o.max_interval < 0) {
                o.max_interval = 0;
            }

            if (o.min_interval && o.min_interval > o.max - o.min) {
                o.min_interval = o.max - o.min;
            }

            if (o.max_interval && o.max_interval > o.max - o.min) {
                o.max_interval = o.max - o.min;
            }
        },

        decorate: function (num, original) {
            var decorated = "",
                o = this.options;

            if (o.prefix) {
                decorated += o.prefix;
            }

            decorated += num;

            if (o.max_postfix) {
                if (o.values.length && num === o.p_values[o.max]) {
                    decorated += o.max_postfix;
                    if (o.postfix) {
                        decorated += " ";
                    }
                } else if (original === o.max) {
                    decorated += o.max_postfix;
                    if (o.postfix) {
                        decorated += " ";
                    }
                }
            }

            if (o.postfix) {
                decorated += o.postfix;
            }

            return decorated;
        },

        updateFrom: function () {
            this.result.from = this.options.from;
            this.result.from_percent = this.calcPercent(this.result.from);
            if (this.options.values) {
                this.result.from_value = this.options.values[this.result.from];
            }
        },

        updateTo: function () {
            this.result.to = this.options.to;
            this.result.to_percent = this.calcPercent(this.result.to);
            if (this.options.values) {
                this.result.to_value = this.options.values[this.result.to];
            }
        },

        updateResult: function () {
            this.result.min = this.options.min;
            this.result.max = this.options.max;
            this.updateFrom();
            this.updateTo();
        },


        // =============================================================================================================
        // Grid

        appendGrid: function () {
            if (!this.options.grid) {
                return;
            }

            var o = this.options,
                i, z,

                total = o.max - o.min,
                big_num = o.grid_num,
                big_p = 0,
                big_w = 0,

                small_max = 4,
                local_small_max,
                small_p,
                small_w = 0,

                result,
                html = '';



            this.calcGridMargin();

            if (o.grid_snap) {
                big_num = total / o.step;
                big_p = this.toFixed(o.step / (total / 100));
            } else {
                big_p = this.toFixed(100 / big_num);
            }

            if (big_num > 4) {
                small_max = 3;
            }
            if (big_num > 7) {
                small_max = 2;
            }
            if (big_num > 14) {
                small_max = 1;
            }
            if (big_num > 28) {
                small_max = 0;
            }

            for (i = 0; i < big_num + 1; i++) {
                local_small_max = small_max;

                big_w = this.toFixed(big_p * i);
                if (big_w > 100) {
                    big_w = 100;

                    local_small_max -= 2;
                    if (local_small_max < 0) {
                        local_small_max = 0;
                    }
                }
                this.coords.big[i] = big_w;

                small_p = (big_w - (big_p * (i - 1))) / (local_small_max + 1);

                for (z = 1; z <= local_small_max; z++) {
                    if (big_w === 0) {
                        break;
                    }

                    small_w = this.toFixed(big_w - (small_p * z));

                    html += '<span class="irs-grid-pol small" style="left: ' + small_w + '%"></span>';
                }

                html += '<span class="irs-grid-pol" style="left: ' + big_w + '%"></span>';

                result = this.calcReal(big_w);
                if (o.values.length) {
                    result = o.p_values[result];
                } else {
                    result = this._prettify(result);
                }

                html += '<span class="irs-grid-text js-grid-text-' + i + '" style="left: ' + big_w + '%">' + result + '</span>';
            }
            this.coords.big_num = Math.ceil(big_num + 1);



            this.$cache.cont.addClass("irs-with-grid");
            this.$cache.grid.html(html);
            this.cacheGridLabels();
        },

        cacheGridLabels: function () {
            var $label, i,
                num = this.coords.big_num;

            for (i = 0; i < num; i++) {
                $label = this.$cache.grid.find(".js-grid-text-" + i);
                this.$cache.grid_labels.push($label);
            }

            this.calcGridLabels();
        },

        calcGridLabels: function () {
            var i, label, start = [], finish = [],
                num = this.coords.big_num;

            for (i = 0; i < num; i++) {
                this.coords.big_w[i] = this.$cache.grid_labels[i].outerWidth(false);
                this.coords.big_p[i] = this.toFixed(this.coords.big_w[i] / this.coords.w_rs * 100);
                this.coords.big_x[i] = this.toFixed(this.coords.big_p[i] / 2);

                start[i] = this.toFixed(this.coords.big[i] - this.coords.big_x[i]);
                finish[i] = this.toFixed(start[i] + this.coords.big_p[i]);
            }

            if (this.options.force_edges) {
                if (start[0] < this.coords.grid_gap) {
                    start[0] = this.coords.grid_gap;
                    finish[0] = this.toFixed(start[0] + this.coords.big_p[0]);

                    this.coords.big_x[0] = this.coords.grid_gap;
                }

                if (finish[num - 1] > 100 - this.coords.grid_gap) {
                    finish[num - 1] = 100 - this.coords.grid_gap;
                    start[num - 1] = this.toFixed(finish[num - 1] - this.coords.big_p[num - 1]);

                    this.coords.big_x[num - 1] = this.toFixed(this.coords.big_p[num - 1] - this.coords.grid_gap);
                }
            }

            this.calcGridCollision(2, start, finish);
            this.calcGridCollision(4, start, finish);

            for (i = 0; i < num; i++) {
                label = this.$cache.grid_labels[i][0];
                label.style.marginLeft = -this.coords.big_x[i] + "%";
            }
        },

        // Collisions Calc Beta
        // TODO: Refactor then have plenty of time
        calcGridCollision: function (step, start, finish) {
            var i, next_i, label,
                num = this.coords.big_num;

            for (i = 0; i < num; i += step) {
                next_i = i + (step / 2);
                if (next_i >= num) {
                    break;
                }

                label = this.$cache.grid_labels[next_i][0];

                if (finish[i] <= start[next_i]) {
                    label.style.visibility = "visible";
                } else {
                    label.style.visibility = "hidden";
                }
            }
        },

        calcGridMargin: function () {
            if (!this.options.grid_margin) {
                return;
            }

            this.coords.w_rs = this.$cache.rs.outerWidth(false);
            if (!this.coords.w_rs) {
                return;
            }

            if (this.options.type === "single") {
                this.coords.w_handle = this.$cache.s_single.outerWidth(false);
            } else {
                this.coords.w_handle = this.$cache.s_from.outerWidth(false);
            }
            this.coords.p_handle = this.toFixed(this.coords.w_handle  / this.coords.w_rs * 100);
            this.coords.grid_gap = this.toFixed((this.coords.p_handle / 2) - 0.1);

            this.$cache.grid[0].style.width = this.toFixed(100 - this.coords.p_handle) + "%";
            this.$cache.grid[0].style.left = this.coords.grid_gap + "%";
        },



        // =============================================================================================================
        // Public methods

        update: function (options) {
            this.is_update = true;
            this.options = $.extend(this.options, options);
            this.validate();
            this.updateResult(options);

            this.toggleInput();
            this.remove();
            this.init(true);
        },

        reset: function () {
            this.updateResult();
            this.update();
        },

        destroy: function () {
            this.toggleInput();
            this.$cache.input.prop("readonly", false);
            $.data(this.input, "ionRangeSlider", null);

            this.remove();
            this.input = null;
            this.options = null;
        }
    };

    $.fn.ionRangeSlider = function (options) {
        return this.each(function() {
            if (!$.data(this, "ionRangeSlider")) {
                $.data(this, "ionRangeSlider", new IonRangeSlider(this, options, plugin_count++));
            }
        });
    };



    // =================================================================================================================
    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

    // MIT license

    (function() {
        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];
        for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
            window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                || window[vendors[x]+'CancelRequestAnimationFrame'];
        }

        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = function(callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                    timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };

        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = function(id) {
                clearTimeout(id);
            };
    }());

} (jQuery, document, window, navigator));

},{}],4:[function(require,module,exports){
// Movie Crop.js
// version 1.0.0 RC
// =====================================================================================================================

var MovieCrop = function (wrap, config) {
    this.config = config;
    this.tm = null;
    this.range = null;
    this.data = null;

    this.$cache = {
        wrap: $(wrap),
        base: null,
        frames: null,
        cover: null,
        ribbon: null,
        input: null
    };

    this.cl = {
        // hooks
        base: ".js-mc",
        frames: ".js-mc-frames",
        cover: ".mc-frames-cover",
        input: ".js-mc-range",
        ribbon: ".js-mc-ribbon",

        // styles
        hidden: "mc-frames-ribbon_state_hidden",
        loading: "mc-frames-cover_state_loading",
        loaded: "mc-frames-cover_state_loaded"
    };

    this.range_config = {
        type: "double",
        min: 0,
        max: this.config.full_length,
        from: this.config.start_from,
        to: this.config.start_from + this.config.crop_length,
        step: 0.001,

        min_interval: this.config.crop_length,
        max_interval: this.config.crop_length,
        drag_interval: true,

        grid: true,
        grid_num: 5,

        hide_min_max: true,
        force_edges: true,

        onStart: this.change.bind(this),
        onFinish: this.change.bind(this),
        onUpdate: this.change.bind(this),

        prettify: function (num) {
            var minutes = Math.floor(num / 60).toString(),
                seconds = Math.floor(num - (minutes * 60)).toString(),
                mili = num.toString().split("."),
                miliseconds = mili.length > 1 ? mili[1] : "000",
                result;

            minutes = minutes.length < 2 ? "0" + minutes : minutes;
            seconds = seconds.length < 2 ? "0" + seconds : seconds;
            miliseconds = miliseconds.length < 2 ? miliseconds + "00" : miliseconds;
            miliseconds = miliseconds.length < 3 ? miliseconds + "0" : miliseconds;

            result = minutes + ":" + seconds + "." + miliseconds;

            return result;
        }
    };

    this.tpl =
        '<div class="mc js-mc">' +
        '<div class="mc-frames js-mc-frames">' +
        '<div class="mc-frames-cover mc-frames-cover_state_loading"></div>' +
        '<div class="mc-frames-range"><input type="text" value="" class="mc-frames-range__input js-mc-range" /></div>' +
        '</div>' +
        '</div>';

    this.tpl_frames_header = '<div class="mc-frames-ribbon mc-frames-ribbon_state_hidden js-mc-ribbon">';
    this.tpl_frames_item = function (frame) {
        return '<div class="mc-frames-ribbon__item" style="background-image: url(' + frame + ');"></div>'
    };
    this.tpl_frames_footer = '</div>';
};



MovieCrop.prototype = {
    init: function () {
        this.$cache.wrap.append(this.tpl);
        this.$cache.base = this.$cache.wrap.find(this.cl.base);
        this.$cache.frames = this.$cache.wrap.find(this.cl.frames);
        this.$cache.cover = this.$cache.wrap.find(this.cl.cover);
        this.$cache.input = this.$cache.wrap.find(this.cl.input);

        if (this.config.frames && this.config.frames.length) {
            this.createFrames();
        }
    },

    createFrames: function () {
        var frames = this.config.frames,
            num = frames.length,
            html = '',
            i;

        html += this.tpl_frames_header;
        for (i = 0; i < num; i++) {
            html += this.tpl_frames_item(frames[i]);
        }
        html += this.tpl_frames_footer;

        this.$cache.frames.prepend(html);
        this.$cache.ribbon = this.$cache.frames.find(this.cl.ribbon);

        this.tm = setTimeout(this.showFrames.bind(this), 500);
    },

    showFrames: function () {
        clearTimeout(this.tm);
        this.tm = null;

        this.$cache.ribbon.removeClass(this.cl.hidden);

        this.tm = setTimeout(this.removeLoader.bind(this), 500);
    },

    removeLoader: function () {
        clearTimeout(this.tm);
        this.tm = null;

        this.$cache.cover.removeClass(this.cl.loading);
        this.tm = setTimeout(this.addMask.bind(this), 100);
    },

    addMask: function () {
        clearTimeout(this.tm);
        this.tm = null;

        this.$cache.cover.addClass(this.cl.loaded);

        this.createRange();
    },

    createRange: function () {
        this.$cache.input.ionRangeSlider(this.range_config);
        this.range = this.$cache.input.data("ionRangeSlider");
    },

    change: function (data) {
        var c = this.config;

        if (data.to !== data.from + c.crop_length) {
            data.to = data.from + c.crop_length;
            data.to = +data.to.toFixed(3);
        }

        if (c.change && typeof c.change === "function") {
            if (c.scope) {
                c.change.call(c.scope, data);
            } else {
                c.change(data);
            }
        }
    }
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(MovieCrop.prototype);
  MovieCrop.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{}],5:[function(require,module,exports){
/*! Video.js v4.11.1 Copyright 2014 Brightcove, Inc. https://github.com/videojs/video.js/blob/master/LICENSE */ 
(function() {var b=void 0,f=!0,k=null,l=!1;function m(){return function(){}}function n(a){return function(){return this[a]}}function r(a){return function(){return a}}var s;document.createElement("video");document.createElement("audio");document.createElement("track");function t(a,c,d){if("string"===typeof a){0===a.indexOf("#")&&(a=a.slice(1));if(t.Fa[a])return t.Fa[a];a=t.w(a)}if(!a||!a.nodeName)throw new TypeError("The element or ID supplied is not valid. (videojs)");return a.player||new t.Player(a,c,d)}
var videojs=window.videojs=t;t.Yb="4.11";t.ed="https:"==document.location.protocol?"https://":"http://";
t.options={techOrder:["html5","flash"],html5:{},flash:{},width:300,height:150,defaultVolume:0,playbackRates:[],inactivityTimeout:2E3,children:{mediaLoader:{},posterImage:{},textTrackDisplay:{},loadingSpinner:{},bigPlayButton:{},controlBar:{},errorDisplay:{}},language:document.getElementsByTagName("html")[0].getAttribute("lang")||navigator.languages&&navigator.languages[0]||navigator.Me||navigator.language||"en",languages:{},notSupportedMessage:"No compatible source was found for this video."};
"GENERATED_CDN_VSN"!==t.Yb&&(videojs.options.flash.swf=t.ed+"vjs.zencdn.net/"+t.Yb+"/video-js.swf");t.sd=function(a,c){t.options.languages[a]=t.options.languages[a]!==b?t.Z.Ea(t.options.languages[a],c):c;return t.options.languages};t.Fa={};"function"===typeof define&&define.amd?define([],function(){return videojs}):"object"===typeof exports&&"object"===typeof module&&(module.exports=videojs);t.ta=t.CoreObject=m();
t.ta.extend=function(a){var c,d;a=a||{};c=a.init||a.i||this.prototype.init||this.prototype.i||m();d=function(){c.apply(this,arguments)};d.prototype=t.h.create(this.prototype);d.prototype.constructor=d;d.extend=t.ta.extend;d.create=t.ta.create;for(var e in a)a.hasOwnProperty(e)&&(d.prototype[e]=a[e]);return d};t.ta.create=function(){var a=t.h.create(this.prototype);this.apply(a,arguments);return a};
t.c=function(a,c,d){if(t.h.isArray(c))return u(t.c,a,c,d);var e=t.getData(a);e.C||(e.C={});e.C[c]||(e.C[c]=[]);d.p||(d.p=t.p++);e.C[c].push(d);e.W||(e.disabled=l,e.W=function(c){if(!e.disabled){c=t.zc(c);var d=e.C[c.type];if(d)for(var d=d.slice(0),j=0,p=d.length;j<p&&!c.Gc();j++)d[j].call(a,c)}});1==e.C[c].length&&(a.addEventListener?a.addEventListener(c,e.W,l):a.attachEvent&&a.attachEvent("on"+c,e.W))};
t.k=function(a,c,d){if(t.Bc(a)){var e=t.getData(a);if(e.C){if(t.h.isArray(c))return u(t.k,a,c,d);if(c){var g=e.C[c];if(g){if(d){if(d.p)for(e=0;e<g.length;e++)g[e].p===d.p&&g.splice(e--,1)}else e.C[c]=[];t.pc(a,c)}}else for(g in e.C)c=g,e.C[c]=[],t.pc(a,c)}}};t.pc=function(a,c){var d=t.getData(a);0===d.C[c].length&&(delete d.C[c],a.removeEventListener?a.removeEventListener(c,d.W,l):a.detachEvent&&a.detachEvent("on"+c,d.W));t.Kb(d.C)&&(delete d.C,delete d.W,delete d.disabled);t.Kb(d)&&t.Pc(a)};
t.zc=function(a){function c(){return f}function d(){return l}if(!a||!a.Lb){var e=a||window.event;a={};for(var g in e)"layerX"!==g&&("layerY"!==g&&"keyLocation"!==g)&&("returnValue"==g&&e.preventDefault||(a[g]=e[g]));a.target||(a.target=a.srcElement||document);a.relatedTarget=a.fromElement===a.target?a.toElement:a.fromElement;a.preventDefault=function(){e.preventDefault&&e.preventDefault();a.returnValue=l;a.Nd=c;a.defaultPrevented=f};a.Nd=d;a.defaultPrevented=l;a.stopPropagation=function(){e.stopPropagation&&
e.stopPropagation();a.cancelBubble=f;a.Lb=c};a.Lb=d;a.stopImmediatePropagation=function(){e.stopImmediatePropagation&&e.stopImmediatePropagation();a.Gc=c;a.stopPropagation()};a.Gc=d;if(a.clientX!=k){g=document.documentElement;var h=document.body;a.pageX=a.clientX+(g&&g.scrollLeft||h&&h.scrollLeft||0)-(g&&g.clientLeft||h&&h.clientLeft||0);a.pageY=a.clientY+(g&&g.scrollTop||h&&h.scrollTop||0)-(g&&g.clientTop||h&&h.clientTop||0)}a.which=a.charCode||a.keyCode;a.button!=k&&(a.button=a.button&1?0:a.button&
4?1:a.button&2?2:0)}return a};t.l=function(a,c){var d=t.Bc(a)?t.getData(a):{},e=a.parentNode||a.ownerDocument;"string"===typeof c&&(c={type:c,target:a});c=t.zc(c);d.W&&d.W.call(a,c);if(e&&!c.Lb()&&c.bubbles!==l)t.l(e,c);else if(!e&&!c.defaultPrevented&&(d=t.getData(c.target),c.target[c.type])){d.disabled=f;if("function"===typeof c.target[c.type])c.target[c.type]();d.disabled=l}return!c.defaultPrevented};
t.Q=function(a,c,d){function e(){t.k(a,c,e);d.apply(this,arguments)}if(t.h.isArray(c))return u(t.Q,a,c,d);e.p=d.p=d.p||t.p++;t.c(a,c,e)};function u(a,c,d,e){t.mc.forEach(d,function(d){a(c,d,e)})}var v=Object.prototype.hasOwnProperty;t.e=function(a,c){var d;c=c||{};d=document.createElement(a||"div");t.h.X(c,function(a,c){-1!==a.indexOf("aria-")||"role"==a?d.setAttribute(a,c):d[a]=c});return d};t.ba=function(a){return a.charAt(0).toUpperCase()+a.slice(1)};t.h={};
t.h.create=Object.create||function(a){function c(){}c.prototype=a;return new c};t.h.X=function(a,c,d){for(var e in a)v.call(a,e)&&c.call(d||this,e,a[e])};t.h.z=function(a,c){if(!c)return a;for(var d in c)v.call(c,d)&&(a[d]=c[d]);return a};t.h.Ad=function(a,c){var d,e,g;a=t.h.copy(a);for(d in c)v.call(c,d)&&(e=a[d],g=c[d],a[d]=t.h.Xa(e)&&t.h.Xa(g)?t.h.Ad(e,g):c[d]);return a};t.h.copy=function(a){return t.h.z({},a)};
t.h.Xa=function(a){return!!a&&"object"===typeof a&&"[object Object]"===a.toString()&&a.constructor===Object};t.h.isArray=Array.isArray||function(a){return"[object Array]"===Object.prototype.toString.call(a)};t.Pd=function(a){return a!==a};t.bind=function(a,c,d){function e(){return c.apply(a,arguments)}c.p||(c.p=t.p++);e.p=d?d+"_"+c.p:c.p;return e};t.xa={};t.p=1;t.expando="vdata"+(new Date).getTime();t.getData=function(a){var c=a[t.expando];c||(c=a[t.expando]=t.p++,t.xa[c]={});return t.xa[c]};
t.Bc=function(a){a=a[t.expando];return!(!a||t.Kb(t.xa[a]))};t.Pc=function(a){var c=a[t.expando];if(c){delete t.xa[c];try{delete a[t.expando]}catch(d){a.removeAttribute?a.removeAttribute(t.expando):a[t.expando]=k}}};t.Kb=function(a){for(var c in a)if(a[c]!==k)return l;return f};t.Wa=function(a,c){return-1!==(" "+a.className+" ").indexOf(" "+c+" ")};t.n=function(a,c){t.Wa(a,c)||(a.className=""===a.className?c:a.className+" "+c)};
t.r=function(a,c){var d,e;if(t.Wa(a,c)){d=a.className.split(" ");for(e=d.length-1;0<=e;e--)d[e]===c&&d.splice(e,1);a.className=d.join(" ")}};t.A=t.e("video");t.N=navigator.userAgent;t.md=/iPhone/i.test(t.N);t.ld=/iPad/i.test(t.N);t.nd=/iPod/i.test(t.N);t.kd=t.md||t.ld||t.nd;var aa=t,x;var y=t.N.match(/OS (\d+)_/i);x=y&&y[1]?y[1]:b;aa.Ae=x;t.hd=/Android/i.test(t.N);var ba=t,z;var A=t.N.match(/Android (\d+)(?:\.(\d+))?(?:\.(\d+))*/i),B,C;
A?(B=A[1]&&parseFloat(A[1]),C=A[2]&&parseFloat(A[2]),z=B&&C?parseFloat(A[1]+"."+A[2]):B?B:k):z=k;ba.Xb=z;t.od=t.hd&&/webkit/i.test(t.N)&&2.3>t.Xb;t.jd=/Firefox/i.test(t.N);t.Be=/Chrome/i.test(t.N);t.ic=!!("ontouchstart"in window||window.gd&&document instanceof window.gd);t.fd="backgroundSize"in t.A.style;t.Sc=function(a,c){t.h.X(c,function(c,e){e===k||"undefined"===typeof e||e===l?a.removeAttribute(c):a.setAttribute(c,e===f?"":e)})};
t.Ca=function(a){var c,d,e,g;c={};if(a&&a.attributes&&0<a.attributes.length){d=a.attributes;for(var h=d.length-1;0<=h;h--){e=d[h].name;g=d[h].value;if("boolean"===typeof a[e]||-1!==",autoplay,controls,loop,muted,default,".indexOf(","+e+","))g=g!==k?f:l;c[e]=g}}return c};
t.He=function(a,c){var d="";document.defaultView&&document.defaultView.getComputedStyle?d=document.defaultView.getComputedStyle(a,"").getPropertyValue(c):a.currentStyle&&(d=a["client"+c.substr(0,1).toUpperCase()+c.substr(1)]+"px");return d};t.Jb=function(a,c){c.firstChild?c.insertBefore(a,c.firstChild):c.appendChild(a)};t.Ra={};t.w=function(a){0===a.indexOf("#")&&(a=a.slice(1));return document.getElementById(a)};
t.Ba=function(a,c){c=c||a;var d=Math.floor(a%60),e=Math.floor(a/60%60),g=Math.floor(a/3600),h=Math.floor(c/60%60),j=Math.floor(c/3600);if(isNaN(a)||Infinity===a)g=e=d="-";g=0<g||0<j?g+":":"";return g+(((g||10<=h)&&10>e?"0"+e:e)+":")+(10>d?"0"+d:d)};t.ud=function(){document.body.focus();document.onselectstart=r(l)};t.ve=function(){document.onselectstart=r(f)};t.trim=function(a){return(a+"").replace(/^\s+|\s+$/g,"")};t.round=function(a,c){c||(c=0);return Math.round(a*Math.pow(10,c))/Math.pow(10,c)};
t.zb=function(a,c){return{length:1,start:function(){return a},end:function(){return c}}};t.je=function(a){try{var c=window.localStorage||l;c&&(c.volume=a)}catch(d){22==d.code||1014==d.code?t.log("LocalStorage Full (VideoJS)",d):18==d.code?t.log("LocalStorage not allowed (VideoJS)",d):t.log("LocalStorage Error (VideoJS)",d)}};t.Jd=function(a){a.match(/^https?:\/\//)||(a=t.e("div",{innerHTML:'<a href="'+a+'">x</a>'}).firstChild.href);return a};
t.fe=function(a){var c,d,e,g;g="protocol hostname port pathname search hash host".split(" ");d=t.e("a",{href:a});if(e=""===d.host&&"file:"!==d.protocol)c=t.e("div"),c.innerHTML='<a href="'+a+'"></a>',d=c.firstChild,c.setAttribute("style","display:none; position:absolute;"),document.body.appendChild(c);a={};for(var h=0;h<g.length;h++)a[g[h]]=d[g[h]];e&&document.body.removeChild(c);return a};
function D(a,c){var d,e;d=Array.prototype.slice.call(c);e=m();e=window.console||{log:e,warn:e,error:e};a?d.unshift(a.toUpperCase()+":"):a="log";t.log.history.push(d);d.unshift("VIDEOJS:");if(e[a].apply)e[a].apply(e,d);else e[a](d.join(" "))}t.log=function(){D(k,arguments)};t.log.history=[];t.log.error=function(){D("error",arguments)};t.log.warn=function(){D("warn",arguments)};
t.Hd=function(a){var c,d;a.getBoundingClientRect&&a.parentNode&&(c=a.getBoundingClientRect());if(!c)return{left:0,top:0};a=document.documentElement;d=document.body;return{left:t.round(c.left+(window.pageXOffset||d.scrollLeft)-(a.clientLeft||d.clientLeft||0)),top:t.round(c.top+(window.pageYOffset||d.scrollTop)-(a.clientTop||d.clientTop||0))}};t.mc={};t.mc.forEach=function(a,c,d){if(t.h.isArray(a)&&c instanceof Function)for(var e=0,g=a.length;e<g;++e)c.call(d||t,a[e],e,a);return a};
t.ye=function(a,c){var d,e,g,h,j,p,q;"string"===typeof a&&(a={uri:a});videojs.Z.Ea({method:"GET",timeout:45E3},a);c=c||m();p=function(){window.clearTimeout(j);c(k,e,e.response||e.responseText)};q=function(a){window.clearTimeout(j);if(!a||"string"===typeof a)a=Error(a);c(a,e)};d=window.XMLHttpRequest;"undefined"===typeof d&&(d=function(){try{return new window.ActiveXObject("Msxml2.XMLHTTP.6.0")}catch(a){}try{return new window.ActiveXObject("Msxml2.XMLHTTP.3.0")}catch(c){}try{return new window.ActiveXObject("Msxml2.XMLHTTP")}catch(d){}throw Error("This browser does not support XMLHttpRequest.");
});e=new d;e.uri=a.uri;d=t.fe(a.uri);g=window.location;d.protocol+d.host!==g.protocol+g.host&&window.XDomainRequest&&!("withCredentials"in e)?(e=new window.XDomainRequest,e.onload=p,e.onerror=q,e.onprogress=m(),e.ontimeout=m()):(h="file:"==d.protocol||"file:"==g.protocol,e.onreadystatechange=function(){if(4===e.readyState){if(e.te)return q("timeout");200===e.status||h&&0===e.status?p():q()}},a.timeout&&(j=window.setTimeout(function(){4!==e.readyState&&(e.te=f,e.abort())},a.timeout)));try{e.open(a.method||
"GET",a.uri,f)}catch(w){q(w);return}a.withCredentials&&(e.withCredentials=f);a.responseType&&(e.responseType=a.responseType);try{e.send()}catch(ja){q(ja)}};t.Z={};t.Z.Ea=function(a,c){var d,e,g;a=t.h.copy(a);for(d in c)c.hasOwnProperty(d)&&(e=a[d],g=c[d],a[d]=t.h.Xa(e)&&t.h.Xa(g)?t.Z.Ea(e,g):c[d]);return a};
t.a=t.ta.extend({i:function(a,c,d){this.d=a;this.m=t.h.copy(this.m);c=this.options(c);this.K=c.id||c.el&&c.el.id;this.K||(this.K=(a.id&&a.id()||"no_player")+"_component_"+t.p++);this.Vd=c.name||k;this.b=c.el||this.e();this.O=[];this.Ta={};this.Ua={};this.Dc();this.H(d);if(c.Qc!==l){var e,g;this.j().reportUserActivity&&(e=t.bind(this.j(),this.j().reportUserActivity),this.c("touchstart",function(){e();this.clearInterval(g);g=this.setInterval(e,250)}),a=function(){e();this.clearInterval(g)},this.c("touchmove",
e),this.c("touchend",a),this.c("touchcancel",a))}}});s=t.a.prototype;s.dispose=function(){this.l({type:"dispose",bubbles:l});if(this.O)for(var a=this.O.length-1;0<=a;a--)this.O[a].dispose&&this.O[a].dispose();this.Ua=this.Ta=this.O=k;this.k();this.b.parentNode&&this.b.parentNode.removeChild(this.b);t.Pc(this.b);this.b=k};s.d=f;s.j=n("d");s.options=function(a){return a===b?this.m:this.m=t.Z.Ea(this.m,a)};s.e=function(a,c){return t.e(a,c)};
s.t=function(a){var c=this.d.language(),d=this.d.languages();return d&&d[c]&&d[c][a]?d[c][a]:a};s.w=n("b");s.la=function(){return this.v||this.b};s.id=n("K");s.name=n("Vd");s.children=n("O");s.Kd=function(a){return this.Ta[a]};s.ma=function(a){return this.Ua[a]};
s.U=function(a,c){var d,e;"string"===typeof a?(e=a,c=c||{},d=c.componentClass||t.ba(e),c.name=e,d=new window.videojs[d](this.d||this,c)):d=a;this.O.push(d);"function"===typeof d.id&&(this.Ta[d.id()]=d);(e=e||d.name&&d.name())&&(this.Ua[e]=d);"function"===typeof d.el&&d.el()&&this.la().appendChild(d.el());return d};
s.removeChild=function(a){"string"===typeof a&&(a=this.ma(a));if(a&&this.O){for(var c=l,d=this.O.length-1;0<=d;d--)if(this.O[d]===a){c=f;this.O.splice(d,1);break}c&&(this.Ta[a.id]=k,this.Ua[a.name]=k,(c=a.w())&&c.parentNode===this.la()&&this.la().removeChild(a.w()))}};
s.Dc=function(){var a,c,d,e,g,h;a=this;c=a.options();if(d=c.children)if(h=function(d,e){c[d]!==b&&(e=c[d]);e!==l&&(a[d]=a.U(d,e))},t.h.isArray(d))for(var j=0;j<d.length;j++)e=d[j],"string"==typeof e?(g=e,e={}):g=e.name,h(g,e);else t.h.X(d,h)};s.S=r("");
s.c=function(a,c,d){var e,g,h;"string"===typeof a||t.h.isArray(a)?t.c(this.b,a,t.bind(this,c)):(e=t.bind(this,d),h=this,g=function(){h.k(a,c,e)},g.p=e.p,this.c("dispose",g),d=function(){h.k("dispose",g)},d.p=e.p,a.nodeName?(t.c(a,c,e),t.c(a,"dispose",d)):"function"===typeof a.c&&(a.c(c,e),a.c("dispose",d)));return this};
s.k=function(a,c,d){!a||"string"===typeof a||t.h.isArray(a)?t.k(this.b,a,c):(d=t.bind(this,d),this.k("dispose",d),a.nodeName?(t.k(a,c,d),t.k(a,"dispose",d)):(a.k(c,d),a.k("dispose",d)));return this};s.Q=function(a,c,d){var e,g,h;"string"===typeof a||t.h.isArray(a)?t.Q(this.b,a,t.bind(this,c)):(e=t.bind(this,d),g=this,h=function(){g.k(a,c,h);e.apply(this,arguments)},h.p=e.p,this.c(a,c,h));return this};s.l=function(a){t.l(this.b,a);return this};
s.H=function(a){a&&(this.na?a.call(this):(this.cb===b&&(this.cb=[]),this.cb.push(a)));return this};s.Ja=function(){this.na=f;var a=this.cb;if(a&&0<a.length){for(var c=0,d=a.length;c<d;c++)a[c].call(this);this.cb=[];this.l("ready")}};s.Wa=function(a){return t.Wa(this.b,a)};s.n=function(a){t.n(this.b,a);return this};s.r=function(a){t.r(this.b,a);return this};s.show=function(){this.b.style.display="block";return this};s.Y=function(){this.b.style.display="none";return this};
function E(a){a.r("vjs-lock-showing")}s.disable=function(){this.Y();this.show=m()};s.width=function(a,c){return F(this,"width",a,c)};s.height=function(a,c){return F(this,"height",a,c)};s.Dd=function(a,c){return this.width(a,f).height(c)};
function F(a,c,d,e){if(d!==b){if(d===k||t.Pd(d))d=0;a.b.style[c]=-1!==(""+d).indexOf("%")||-1!==(""+d).indexOf("px")?d:"auto"===d?"":d+"px";e||a.l("resize");return a}if(!a.b)return 0;d=a.b.style[c];e=d.indexOf("px");return-1!==e?parseInt(d.slice(0,e),10):parseInt(a.b["offset"+t.ba(c)],10)}
function G(a){var c,d,e,g,h,j,p,q;c=0;d=k;a.c("touchstart",function(a){1===a.touches.length&&(d=a.touches[0],c=(new Date).getTime(),g=f)});a.c("touchmove",function(a){1<a.touches.length?g=l:d&&(j=a.touches[0].pageX-d.pageX,p=a.touches[0].pageY-d.pageY,q=Math.sqrt(j*j+p*p),22<q&&(g=l))});h=function(){g=l};a.c("touchleave",h);a.c("touchcancel",h);a.c("touchend",function(a){d=k;g===f&&(e=(new Date).getTime()-c,250>e&&(a.preventDefault(),this.l("tap")))})}
s.setTimeout=function(a,c){function d(){this.clearTimeout(e)}a=t.bind(this,a);var e=setTimeout(a,c);d.p="vjs-timeout-"+e;this.c("dispose",d);return e};s.clearTimeout=function(a){function c(){}clearTimeout(a);c.p="vjs-timeout-"+a;this.k("dispose",c);return a};s.setInterval=function(a,c){function d(){this.clearInterval(e)}a=t.bind(this,a);var e=setInterval(a,c);d.p="vjs-interval-"+e;this.c("dispose",d);return e};
s.clearInterval=function(a){function c(){}clearInterval(a);c.p="vjs-interval-"+a;this.k("dispose",c);return a};t.u=t.a.extend({i:function(a,c){t.a.call(this,a,c);G(this);this.c("tap",this.s);this.c("click",this.s);this.c("focus",this.ab);this.c("blur",this.$a)}});s=t.u.prototype;
s.e=function(a,c){var d;c=t.h.z({className:this.S(),role:"button","aria-live":"polite",tabIndex:0},c);d=t.a.prototype.e.call(this,a,c);c.innerHTML||(this.v=t.e("div",{className:"vjs-control-content"}),this.xb=t.e("span",{className:"vjs-control-text",innerHTML:this.t(this.wa)||"Need Text"}),this.v.appendChild(this.xb),d.appendChild(this.v));return d};s.S=function(){return"vjs-control "+t.a.prototype.S.call(this)};s.s=m();s.ab=function(){t.c(document,"keydown",t.bind(this,this.ea))};
s.ea=function(a){if(32==a.which||13==a.which)a.preventDefault(),this.s()};s.$a=function(){t.k(document,"keydown",t.bind(this,this.ea))};t.R=t.a.extend({i:function(a,c){t.a.call(this,a,c);this.td=this.ma(this.m.barName);this.handle=this.ma(this.m.handleName);this.c("mousedown",this.bb);this.c("touchstart",this.bb);this.c("focus",this.ab);this.c("blur",this.$a);this.c("click",this.s);this.c(a,"controlsvisible",this.update);this.c(a,this.Lc,this.update)}});s=t.R.prototype;
s.e=function(a,c){c=c||{};c.className+=" vjs-slider";c=t.h.z({role:"slider","aria-valuenow":0,"aria-valuemin":0,"aria-valuemax":100,tabIndex:0},c);return t.a.prototype.e.call(this,a,c)};s.bb=function(a){a.preventDefault();t.ud();this.n("vjs-sliding");this.c(document,"mousemove",this.fa);this.c(document,"mouseup",this.pa);this.c(document,"touchmove",this.fa);this.c(document,"touchend",this.pa);this.fa(a)};s.fa=m();
s.pa=function(){t.ve();this.r("vjs-sliding");this.k(document,"mousemove",this.fa);this.k(document,"mouseup",this.pa);this.k(document,"touchmove",this.fa);this.k(document,"touchend",this.pa);this.update()};s.update=function(){if(this.b){var a,c=this.Hb(),d=this.handle,e=this.td;isNaN(c)&&(c=0);a=c;if(d){a=this.b.offsetWidth;var g=d.w().offsetWidth;a=g?g/a:0;c*=1-a;a=c+a/2;d.w().style.left=t.round(100*c,2)+"%"}e&&(e.w().style.width=t.round(100*a,2)+"%")}};
function H(a,c){var d,e,g,h;d=a.b;e=t.Hd(d);h=g=d.offsetWidth;d=a.handle;if(a.options().vertical)return h=e.top,e=c.changedTouches?c.changedTouches[0].pageY:c.pageY,d&&(d=d.w().offsetHeight,h+=d/2,g-=d),Math.max(0,Math.min(1,(h-e+g)/g));g=e.left;e=c.changedTouches?c.changedTouches[0].pageX:c.pageX;d&&(d=d.w().offsetWidth,g+=d/2,h-=d);return Math.max(0,Math.min(1,(e-g)/h))}s.ab=function(){this.c(document,"keydown",this.ea)};
s.ea=function(a){if(37==a.which||40==a.which)a.preventDefault(),this.Xc();else if(38==a.which||39==a.which)a.preventDefault(),this.Yc()};s.$a=function(){this.k(document,"keydown",this.ea)};s.s=function(a){a.stopImmediatePropagation();a.preventDefault()};t.$=t.a.extend();t.$.prototype.defaultValue=0;t.$.prototype.e=function(a,c){c=c||{};c.className+=" vjs-slider-handle";c=t.h.z({innerHTML:'<span class="vjs-control-text">'+this.defaultValue+"</span>"},c);return t.a.prototype.e.call(this,"div",c)};
t.ja=t.a.extend();function ca(a,c){a.U(c);c.c("click",t.bind(a,function(){E(this)}))}t.ja.prototype.e=function(){var a=this.options().rc||"ul";this.v=t.e(a,{className:"vjs-menu-content"});a=t.a.prototype.e.call(this,"div",{append:this.v,className:"vjs-menu"});a.appendChild(this.v);t.c(a,"click",function(a){a.preventDefault();a.stopImmediatePropagation()});return a};t.J=t.u.extend({i:function(a,c){t.u.call(this,a,c);this.selected(c.selected)}});
t.J.prototype.e=function(a,c){return t.u.prototype.e.call(this,"li",t.h.z({className:"vjs-menu-item",innerHTML:this.t(this.m.label)},c))};t.J.prototype.s=function(){this.selected(f)};t.J.prototype.selected=function(a){a?(this.n("vjs-selected"),this.b.setAttribute("aria-selected",f)):(this.r("vjs-selected"),this.b.setAttribute("aria-selected",l))};
t.M=t.u.extend({i:function(a,c){t.u.call(this,a,c);this.Da=this.za();this.U(this.Da);this.P&&0===this.P.length&&this.Y();this.c("keydown",this.ea);this.b.setAttribute("aria-haspopup",f);this.b.setAttribute("role","button")}});s=t.M.prototype;s.va=l;s.za=function(){var a=new t.ja(this.d);this.options().title&&a.la().appendChild(t.e("li",{className:"vjs-menu-title",innerHTML:t.ba(this.options().title),re:-1}));if(this.P=this.createItems())for(var c=0;c<this.P.length;c++)ca(a,this.P[c]);return a};
s.ya=m();s.S=function(){return this.className+" vjs-menu-button "+t.u.prototype.S.call(this)};s.ab=m();s.$a=m();s.s=function(){this.Q("mouseout",t.bind(this,function(){E(this.Da);this.b.blur()}));this.va?I(this):J(this)};s.ea=function(a){a.preventDefault();32==a.which||13==a.which?this.va?I(this):J(this):27==a.which&&this.va&&I(this)};function J(a){a.va=f;a.Da.n("vjs-lock-showing");a.b.setAttribute("aria-pressed",f);a.P&&0<a.P.length&&a.P[0].w().focus()}
function I(a){a.va=l;E(a.Da);a.b.setAttribute("aria-pressed",l)}t.D=function(a){"number"===typeof a?this.code=a:"string"===typeof a?this.message=a:"object"===typeof a&&t.h.z(this,a);this.message||(this.message=t.D.Bd[this.code]||"")};t.D.prototype.code=0;t.D.prototype.message="";t.D.prototype.status=k;t.D.Va="MEDIA_ERR_CUSTOM MEDIA_ERR_ABORTED MEDIA_ERR_NETWORK MEDIA_ERR_DECODE MEDIA_ERR_SRC_NOT_SUPPORTED MEDIA_ERR_ENCRYPTED".split(" ");
t.D.Bd={1:"You aborted the video playback",2:"A network error caused the video download to fail part-way.",3:"The video playback was aborted due to a corruption problem or because the video used features your browser did not support.",4:"The video could not be loaded, either because the server or network failed or because the format is not supported.",5:"The video is encrypted and we do not have the keys to decrypt it."};for(var K=0;K<t.D.Va.length;K++)t.D[t.D.Va[K]]=K,t.D.prototype[t.D.Va[K]]=K;
var L,M,N,O;
L=["requestFullscreen exitFullscreen fullscreenElement fullscreenEnabled fullscreenchange fullscreenerror".split(" "),"webkitRequestFullscreen webkitExitFullscreen webkitFullscreenElement webkitFullscreenEnabled webkitfullscreenchange webkitfullscreenerror".split(" "),"webkitRequestFullScreen webkitCancelFullScreen webkitCurrentFullScreenElement webkitCancelFullScreen webkitfullscreenchange webkitfullscreenerror".split(" "),"mozRequestFullScreen mozCancelFullScreen mozFullScreenElement mozFullScreenEnabled mozfullscreenchange mozfullscreenerror".split(" "),"msRequestFullscreen msExitFullscreen msFullscreenElement msFullscreenEnabled MSFullscreenChange MSFullscreenError".split(" ")];
M=L[0];for(O=0;O<L.length;O++)if(L[O][1]in document){N=L[O];break}if(N){t.Ra.Gb={};for(O=0;O<N.length;O++)t.Ra.Gb[M[O]]=N[O]}
t.Player=t.a.extend({i:function(a,c,d){this.I=a;a.id=a.id||"vjs_video_"+t.p++;this.se=a&&t.Ca(a);c=t.h.z(da(a),c);this.Ya=c.language||t.options.language;this.Td=c.languages||t.options.languages;this.F={};this.Mc=c.poster||"";this.yb=!!c.controls;a.controls=l;c.Qc=l;P(this,"audio"===this.I.nodeName.toLowerCase());t.a.call(this,this,c,d);this.controls()?this.n("vjs-controls-enabled"):this.n("vjs-controls-disabled");P(this)&&this.n("vjs-audio");t.Fa[this.K]=this;c.plugins&&t.h.X(c.plugins,function(a,
c){this[a](c)},this);var e,g,h,j,p;e=t.bind(this,this.reportUserActivity);this.c("mousedown",function(){e();this.clearInterval(g);g=this.setInterval(e,250)});this.c("mousemove",function(a){if(a.screenX!=j||a.screenY!=p)j=a.screenX,p=a.screenY,e()});this.c("mouseup",function(){e();this.clearInterval(g)});this.c("keydown",e);this.c("keyup",e);this.setInterval(function(){if(this.sa){this.sa=l;this.userActive(f);this.clearTimeout(h);var a=this.options().inactivityTimeout;0<a&&(h=this.setTimeout(function(){this.sa||
this.userActive(l)},a))}},250)}});s=t.Player.prototype;s.language=function(a){if(a===b)return this.Ya;this.Ya=a;return this};s.languages=n("Td");s.m=t.options;s.dispose=function(){this.l("dispose");this.k("dispose");t.Fa[this.K]=k;this.I&&this.I.player&&(this.I.player=k);this.b&&this.b.player&&(this.b.player=k);this.o&&this.o.dispose();t.a.prototype.dispose.call(this)};
function da(a){var c,d,e={sources:[],tracks:[]};c=t.Ca(a);d=c["data-setup"];d!==k&&t.h.z(c,t.JSON.parse(d||"{}"));t.h.z(e,c);if(a.hasChildNodes()){var g,h;a=a.childNodes;g=0;for(h=a.length;g<h;g++)c=a[g],d=c.nodeName.toLowerCase(),"source"===d?e.sources.push(t.Ca(c)):"track"===d&&e.tracks.push(t.Ca(c))}return e}
s.e=function(){var a=this.b=t.a.prototype.e.call(this,"div"),c=this.I,d;c.removeAttribute("width");c.removeAttribute("height");if(c.hasChildNodes()){var e,g,h,j,p;e=c.childNodes;g=e.length;for(p=[];g--;)h=e[g],j=h.nodeName.toLowerCase(),"track"===j&&p.push(h);for(e=0;e<p.length;e++)c.removeChild(p[e])}d=t.Ca(c);t.h.X(d,function(c){"class"==c?a.className=d[c]:a.setAttribute(c,d[c])});c.id+="_html5_api";c.className="vjs-tech";c.player=a.player=this;this.n("vjs-paused");this.width(this.m.width,f);this.height(this.m.height,
f);c.Md=c.networkState;c.parentNode&&c.parentNode.insertBefore(a,c);t.Jb(c,a);this.b=a;this.c("loadstart",this.Zd);this.c("waiting",this.ee);this.c(["canplay","canplaythrough","playing","ended"],this.de);this.c("seeking",this.be);this.c("seeked",this.ae);this.c("ended",this.Wd);this.c("play",this.Pb);this.c("firstplay",this.Xd);this.c("pause",this.Ob);this.c("progress",this.$d);this.c("durationchange",this.Jc);this.c("fullscreenchange",this.Yd);return a};
function Q(a,c,d){a.o&&(a.na=l,a.o.dispose(),a.o=l);"Html5"!==c&&a.I&&(t.g.Bb(a.I),a.I=k);a.gb=c;a.na=l;var e=t.h.z({source:d,parentEl:a.b},a.m[c.toLowerCase()]);d&&(a.uc=d.type,d.src==a.F.src&&0<a.F.currentTime&&(e.startTime=a.F.currentTime),a.F.src=d.src);a.o=new window.videojs[c](a,e);a.o.H(function(){this.d.Ja()})}s.Zd=function(){this.error(k);this.paused()?(R(this,l),this.Q("play",function(){R(this,f)})):this.l("firstplay")};s.Cc=l;
function R(a,c){c!==b&&a.Cc!==c&&((a.Cc=c)?(a.n("vjs-has-started"),a.l("firstplay")):a.r("vjs-has-started"))}s.Pb=function(){this.r("vjs-paused");this.n("vjs-playing")};s.ee=function(){this.n("vjs-waiting")};s.de=function(){this.r("vjs-waiting")};s.be=function(){this.n("vjs-seeking")};s.ae=function(){this.r("vjs-seeking")};s.Xd=function(){this.m.starttime&&this.currentTime(this.m.starttime);this.n("vjs-has-started")};s.Ob=function(){this.r("vjs-playing");this.n("vjs-paused")};
s.$d=function(){1==this.bufferedPercent()&&this.l("loadedalldata")};s.Wd=function(){this.m.loop?(this.currentTime(0),this.play()):this.paused()||this.pause()};s.Jc=function(){var a=S(this,"duration");a&&(0>a&&(a=Infinity),this.duration(a),Infinity===a?this.n("vjs-live"):this.r("vjs-live"))};s.Yd=function(){this.isFullscreen()?this.n("vjs-fullscreen"):this.r("vjs-fullscreen")};function T(a,c,d){if(a.o&&!a.o.na)a.o.H(function(){this[c](d)});else try{a.o[c](d)}catch(e){throw t.log(e),e;}}
function S(a,c){if(a.o&&a.o.na)try{return a.o[c]()}catch(d){throw a.o[c]===b?t.log("Video.js: "+c+" method not defined for "+a.gb+" playback technology.",d):"TypeError"==d.name?(t.log("Video.js: "+c+" unavailable on "+a.gb+" playback technology element.",d),a.o.na=l):t.log(d),d;}}s.play=function(){T(this,"play");return this};s.pause=function(){T(this,"pause");return this};s.paused=function(){return S(this,"paused")===l?l:f};
s.currentTime=function(a){return a!==b?(T(this,"setCurrentTime",a),this):this.F.currentTime=S(this,"currentTime")||0};s.duration=function(a){if(a!==b)return this.F.duration=parseFloat(a),this;this.F.duration===b&&this.Jc();return this.F.duration||0};s.remainingTime=function(){return this.duration()-this.currentTime()};s.buffered=function(){var a=S(this,"buffered");if(!a||!a.length)a=t.zb(0,0);return a};
s.bufferedPercent=function(){var a=this.duration(),c=this.buffered(),d=0,e,g;if(!a)return 0;for(var h=0;h<c.length;h++)e=c.start(h),g=c.end(h),g>a&&(g=a),d+=g-e;return d/a};s.volume=function(a){if(a!==b)return a=Math.max(0,Math.min(1,parseFloat(a))),this.F.volume=a,T(this,"setVolume",a),t.je(a),this;a=parseFloat(S(this,"volume"));return isNaN(a)?1:a};s.muted=function(a){return a!==b?(T(this,"setMuted",a),this):S(this,"muted")||l};s.Ha=function(){return S(this,"supportsFullScreen")||l};s.Fc=l;
s.isFullscreen=function(a){return a!==b?(this.Fc=!!a,this):this.Fc};s.isFullScreen=function(a){t.log.warn('player.isFullScreen() has been deprecated, use player.isFullscreen() with a lowercase "s")');return this.isFullscreen(a)};
s.requestFullscreen=function(){var a=t.Ra.Gb;this.isFullscreen(f);a?(t.c(document,a.fullscreenchange,t.bind(this,function(c){this.isFullscreen(document[a.fullscreenElement]);this.isFullscreen()===l&&t.k(document,a.fullscreenchange,arguments.callee);this.l("fullscreenchange")})),this.b[a.requestFullscreen]()):this.o.Ha()?T(this,"enterFullScreen"):(this.yc(),this.l("fullscreenchange"));return this};
s.requestFullScreen=function(){t.log.warn('player.requestFullScreen() has been deprecated, use player.requestFullscreen() with a lowercase "s")');return this.requestFullscreen()};s.exitFullscreen=function(){var a=t.Ra.Gb;this.isFullscreen(l);if(a)document[a.exitFullscreen]();else this.o.Ha()?T(this,"exitFullScreen"):(this.Db(),this.l("fullscreenchange"));return this};s.cancelFullScreen=function(){t.log.warn("player.cancelFullScreen() has been deprecated, use player.exitFullscreen()");return this.exitFullscreen()};
s.yc=function(){this.Od=f;this.Ed=document.documentElement.style.overflow;t.c(document,"keydown",t.bind(this,this.Ac));document.documentElement.style.overflow="hidden";t.n(document.body,"vjs-full-window");this.l("enterFullWindow")};s.Ac=function(a){27===a.keyCode&&(this.isFullscreen()===f?this.exitFullscreen():this.Db())};s.Db=function(){this.Od=l;t.k(document,"keydown",this.Ac);document.documentElement.style.overflow=this.Ed;t.r(document.body,"vjs-full-window");this.l("exitFullWindow")};
s.selectSource=function(a){for(var c=0,d=this.m.techOrder;c<d.length;c++){var e=t.ba(d[c]),g=window.videojs[e];if(g){if(g.isSupported())for(var h=0,j=a;h<j.length;h++){var p=j[h];if(g.canPlaySource(p))return{source:p,o:e}}}else t.log.error('The "'+e+'" tech is undefined. Skipped browser support check for that tech.')}return l};
s.src=function(a){if(a===b)return S(this,"src");t.h.isArray(a)?U(this,a):"string"===typeof a?this.src({src:a}):a instanceof Object&&(a.type&&!window.videojs[this.gb].canPlaySource(a)?U(this,[a]):(this.F.src=a.src,this.uc=a.type||"",this.H(function(){this.o.setSource?T(this,"setSource",a):T(this,"src",a.src);"auto"==this.m.preload&&this.load();this.m.autoplay&&this.play()})));return this};
function U(a,c){var d=a.selectSource(c);d?d.o===a.gb?a.src(d.source):Q(a,d.o,d.source):(a.setTimeout(function(){this.error({code:4,message:this.t(this.options().notSupportedMessage)})},0),a.Ja())}s.load=function(){T(this,"load");return this};s.currentSrc=function(){return S(this,"currentSrc")||this.F.src||""};s.zd=function(){return this.uc||""};s.Ga=function(a){return a!==b?(T(this,"setPreload",a),this.m.preload=a,this):S(this,"preload")};
s.autoplay=function(a){return a!==b?(T(this,"setAutoplay",a),this.m.autoplay=a,this):S(this,"autoplay")};s.loop=function(a){return a!==b?(T(this,"setLoop",a),this.m.loop=a,this):S(this,"loop")};s.poster=function(a){if(a===b)return this.Mc;a||(a="");this.Mc=a;T(this,"setPoster",a);this.l("posterchange");return this};
s.controls=function(a){return a!==b?(a=!!a,this.yb!==a&&((this.yb=a)?(this.r("vjs-controls-disabled"),this.n("vjs-controls-enabled"),this.l("controlsenabled")):(this.r("vjs-controls-enabled"),this.n("vjs-controls-disabled"),this.l("controlsdisabled"))),this):this.yb};t.Player.prototype.Wb;s=t.Player.prototype;
s.usingNativeControls=function(a){return a!==b?(a=!!a,this.Wb!==a&&((this.Wb=a)?(this.n("vjs-using-native-controls"),this.l("usingnativecontrols")):(this.r("vjs-using-native-controls"),this.l("usingcustomcontrols"))),this):this.Wb};s.da=k;s.error=function(a){if(a===b)return this.da;if(a===k)return this.da=a,this.r("vjs-error"),this;this.da=a instanceof t.D?a:new t.D(a);this.l("error");this.n("vjs-error");t.log.error("(CODE:"+this.da.code+" "+t.D.Va[this.da.code]+")",this.da.message,this.da);return this};
s.ended=function(){return S(this,"ended")};s.seeking=function(){return S(this,"seeking")};s.sa=f;s.reportUserActivity=function(){this.sa=f};s.Vb=f;s.userActive=function(a){return a!==b?(a=!!a,a!==this.Vb&&((this.Vb=a)?(this.sa=f,this.r("vjs-user-inactive"),this.n("vjs-user-active"),this.l("useractive")):(this.sa=l,this.o&&this.o.Q("mousemove",function(a){a.stopPropagation();a.preventDefault()}),this.r("vjs-user-active"),this.n("vjs-user-inactive"),this.l("userinactive"))),this):this.Vb};
s.playbackRate=function(a){return a!==b?(T(this,"setPlaybackRate",a),this):this.o&&this.o.featuresPlaybackRate?S(this,"playbackRate"):1};s.Ec=l;function P(a,c){return c!==b?(a.Ec=!!c,a):a.Ec}t.Ma=t.a.extend();t.Ma.prototype.m={Ie:"play",children:{playToggle:{},currentTimeDisplay:{},timeDivider:{},durationDisplay:{},remainingTimeDisplay:{},liveDisplay:{},progressControl:{},fullscreenToggle:{},volumeControl:{},muteToggle:{},playbackRateMenuButton:{}}};t.Ma.prototype.e=function(){return t.e("div",{className:"vjs-control-bar"})};
t.ac=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});t.ac.prototype.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-live-controls vjs-control"});this.v=t.e("div",{className:"vjs-live-display",innerHTML:'<span class="vjs-control-text">'+this.t("Stream Type")+"</span>"+this.t("LIVE"),"aria-live":"off"});a.appendChild(this.v);return a};t.dc=t.u.extend({i:function(a,c){t.u.call(this,a,c);this.c(a,"play",this.Pb);this.c(a,"pause",this.Ob)}});s=t.dc.prototype;s.wa="Play";
s.S=function(){return"vjs-play-control "+t.u.prototype.S.call(this)};s.s=function(){this.d.paused()?this.d.play():this.d.pause()};s.Pb=function(){this.r("vjs-paused");this.n("vjs-playing");this.b.children[0].children[0].innerHTML=this.t("Pause")};s.Ob=function(){this.r("vjs-playing");this.n("vjs-paused");this.b.children[0].children[0].innerHTML=this.t("Play")};t.jb=t.a.extend({i:function(a,c){t.a.call(this,a,c);this.c(a,"timeupdate",this.ia)}});
t.jb.prototype.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-current-time vjs-time-controls vjs-control"});this.v=t.e("div",{className:"vjs-current-time-display",innerHTML:'<span class="vjs-control-text">Current Time </span>0:00',"aria-live":"off"});a.appendChild(this.v);return a};t.jb.prototype.ia=function(){var a=this.d.eb?this.d.F.currentTime:this.d.currentTime();this.v.innerHTML='<span class="vjs-control-text">'+this.t("Current Time")+"</span> "+t.Ba(a,this.d.duration())};
t.kb=t.a.extend({i:function(a,c){t.a.call(this,a,c);this.c(a,"timeupdate",this.ia)}});t.kb.prototype.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-duration vjs-time-controls vjs-control"});this.v=t.e("div",{className:"vjs-duration-display",innerHTML:'<span class="vjs-control-text">'+this.t("Duration Time")+"</span> 0:00","aria-live":"off"});a.appendChild(this.v);return a};
t.kb.prototype.ia=function(){var a=this.d.duration();a&&(this.v.innerHTML='<span class="vjs-control-text">'+this.t("Duration Time")+"</span> "+t.Ba(a))};t.kc=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});t.kc.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-time-divider",innerHTML:"<div><span>/</span></div>"})};t.rb=t.a.extend({i:function(a,c){t.a.call(this,a,c);this.c(a,"timeupdate",this.ia)}});
t.rb.prototype.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-remaining-time vjs-time-controls vjs-control"});this.v=t.e("div",{className:"vjs-remaining-time-display",innerHTML:'<span class="vjs-control-text">'+this.t("Remaining Time")+"</span> -0:00","aria-live":"off"});a.appendChild(this.v);return a};t.rb.prototype.ia=function(){this.d.duration()&&(this.v.innerHTML='<span class="vjs-control-text">'+this.t("Remaining Time")+"</span> -"+t.Ba(this.d.remainingTime()))};
t.Na=t.u.extend({i:function(a,c){t.u.call(this,a,c)}});t.Na.prototype.wa="Fullscreen";t.Na.prototype.S=function(){return"vjs-fullscreen-control "+t.u.prototype.S.call(this)};t.Na.prototype.s=function(){this.d.isFullscreen()?(this.d.exitFullscreen(),this.xb.innerHTML=this.t("Fullscreen")):(this.d.requestFullscreen(),this.xb.innerHTML=this.t("Non-Fullscreen"))};t.qb=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});t.qb.prototype.m={children:{seekBar:{}}};
t.qb.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-progress-control vjs-control"})};t.gc=t.R.extend({i:function(a,c){t.R.call(this,a,c);this.c(a,"timeupdate",this.ra);a.H(t.bind(this,this.ra))}});s=t.gc.prototype;s.m={children:{loadProgressBar:{},playProgressBar:{},seekHandle:{}},barName:"playProgressBar",handleName:"seekHandle"};s.Lc="timeupdate";s.e=function(){return t.R.prototype.e.call(this,"div",{className:"vjs-progress-holder","aria-label":"video progress bar"})};
s.ra=function(){var a=this.d.eb?this.d.F.currentTime:this.d.currentTime();this.b.setAttribute("aria-valuenow",t.round(100*this.Hb(),2));this.b.setAttribute("aria-valuetext",t.Ba(a,this.d.duration()))};s.Hb=function(){return this.d.currentTime()/this.d.duration()};s.bb=function(a){t.R.prototype.bb.call(this,a);this.d.eb=f;this.xe=!this.d.paused();this.d.pause()};s.fa=function(a){a=H(this,a)*this.d.duration();a==this.d.duration()&&(a-=0.1);this.d.currentTime(a)};
s.pa=function(a){t.R.prototype.pa.call(this,a);this.d.eb=l;this.xe&&this.d.play()};s.Yc=function(){this.d.currentTime(this.d.currentTime()+5)};s.Xc=function(){this.d.currentTime(this.d.currentTime()-5)};t.nb=t.a.extend({i:function(a,c){t.a.call(this,a,c);this.c(a,"progress",this.update)}});t.nb.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-load-progress",innerHTML:'<span class="vjs-control-text"><span>'+this.t("Loaded")+"</span>: 0%</span>"})};
t.nb.prototype.update=function(){var a,c,d,e,g=this.d.buffered();a=this.d.duration();var h,j=this.d;h=j.buffered();j=j.duration();h=h.end(h.length-1);h>j&&(h=j);j=this.b.children;this.b.style.width=100*(h/a||0)+"%";for(a=0;a<g.length;a++)c=g.start(a),d=g.end(a),(e=j[a])||(e=this.b.appendChild(t.e())),e.style.left=100*(c/h||0)+"%",e.style.width=100*((d-c)/h||0)+"%";for(a=j.length;a>g.length;a--)this.b.removeChild(j[a-1])};t.cc=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});
t.cc.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-play-progress",innerHTML:'<span class="vjs-control-text"><span>'+this.t("Progress")+"</span>: 0%</span>"})};t.Oa=t.$.extend({i:function(a,c){t.$.call(this,a,c);this.c(a,"timeupdate",this.ia)}});t.Oa.prototype.defaultValue="00:00";t.Oa.prototype.e=function(){return t.$.prototype.e.call(this,"div",{className:"vjs-seek-handle","aria-live":"off"})};
t.Oa.prototype.ia=function(){var a=this.d.eb?this.d.F.currentTime:this.d.currentTime();this.b.innerHTML='<span class="vjs-control-text">'+t.Ba(a,this.d.duration())+"</span>"};t.tb=t.a.extend({i:function(a,c){t.a.call(this,a,c);a.o&&a.o.featuresVolumeControl===l&&this.n("vjs-hidden");this.c(a,"loadstart",function(){a.o.featuresVolumeControl===l?this.n("vjs-hidden"):this.r("vjs-hidden")})}});t.tb.prototype.m={children:{volumeBar:{}}};
t.tb.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-volume-control vjs-control"})};t.sb=t.R.extend({i:function(a,c){t.R.call(this,a,c);this.c(a,"volumechange",this.ra);a.H(t.bind(this,this.ra))}});s=t.sb.prototype;s.ra=function(){this.b.setAttribute("aria-valuenow",t.round(100*this.d.volume(),2));this.b.setAttribute("aria-valuetext",t.round(100*this.d.volume(),2)+"%")};s.m={children:{volumeLevel:{},volumeHandle:{}},barName:"volumeLevel",handleName:"volumeHandle"};
s.Lc="volumechange";s.e=function(){return t.R.prototype.e.call(this,"div",{className:"vjs-volume-bar","aria-label":"volume level"})};s.fa=function(a){this.d.muted()&&this.d.muted(l);this.d.volume(H(this,a))};s.Hb=function(){return this.d.muted()?0:this.d.volume()};s.Yc=function(){this.d.volume(this.d.volume()+0.1)};s.Xc=function(){this.d.volume(this.d.volume()-0.1)};t.lc=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});
t.lc.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-volume-level",innerHTML:'<span class="vjs-control-text"></span>'})};t.ub=t.$.extend();t.ub.prototype.defaultValue="00:00";t.ub.prototype.e=function(){return t.$.prototype.e.call(this,"div",{className:"vjs-volume-handle"})};
t.ka=t.u.extend({i:function(a,c){t.u.call(this,a,c);this.c(a,"volumechange",this.update);a.o&&a.o.featuresVolumeControl===l&&this.n("vjs-hidden");this.c(a,"loadstart",function(){a.o.featuresVolumeControl===l?this.n("vjs-hidden"):this.r("vjs-hidden")})}});t.ka.prototype.e=function(){return t.u.prototype.e.call(this,"div",{className:"vjs-mute-control vjs-control",innerHTML:'<div><span class="vjs-control-text">'+this.t("Mute")+"</span></div>"})};
t.ka.prototype.s=function(){this.d.muted(this.d.muted()?l:f)};t.ka.prototype.update=function(){var a=this.d.volume(),c=3;0===a||this.d.muted()?c=0:0.33>a?c=1:0.67>a&&(c=2);this.d.muted()?this.b.children[0].children[0].innerHTML!=this.t("Unmute")&&(this.b.children[0].children[0].innerHTML=this.t("Unmute")):this.b.children[0].children[0].innerHTML!=this.t("Mute")&&(this.b.children[0].children[0].innerHTML=this.t("Mute"));for(a=0;4>a;a++)t.r(this.b,"vjs-vol-"+a);t.n(this.b,"vjs-vol-"+c)};
t.ua=t.M.extend({i:function(a,c){t.M.call(this,a,c);this.c(a,"volumechange",this.update);a.o&&a.o.featuresVolumeControl===l&&this.n("vjs-hidden");this.c(a,"loadstart",function(){a.o.featuresVolumeControl===l?this.n("vjs-hidden"):this.r("vjs-hidden")});this.n("vjs-menu-button")}});t.ua.prototype.za=function(){var a=new t.ja(this.d,{rc:"div"}),c=new t.sb(this.d,this.m.volumeBar);c.c("focus",function(){a.n("vjs-lock-showing")});c.c("blur",function(){E(a)});a.U(c);return a};
t.ua.prototype.s=function(){t.ka.prototype.s.call(this);t.M.prototype.s.call(this)};t.ua.prototype.e=function(){return t.u.prototype.e.call(this,"div",{className:"vjs-volume-menu-button vjs-menu-button vjs-control",innerHTML:'<div><span class="vjs-control-text">'+this.t("Mute")+"</span></div>"})};t.ua.prototype.update=t.ka.prototype.update;t.ec=t.M.extend({i:function(a,c){t.M.call(this,a,c);this.bd();this.ad();this.c(a,"loadstart",this.bd);this.c(a,"ratechange",this.ad)}});s=t.ec.prototype;
s.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-playback-rate vjs-menu-button vjs-control",innerHTML:'<div class="vjs-control-content"><span class="vjs-control-text">'+this.t("Playback Rate")+"</span></div>"});this.Hc=t.e("div",{className:"vjs-playback-rate-value",innerHTML:1});a.appendChild(this.Hc);return a};s.za=function(){var a=new t.ja(this.j()),c=this.j().options().playbackRates;if(c)for(var d=c.length-1;0<=d;d--)a.U(new t.pb(this.j(),{rate:c[d]+"x"}));return a};
s.ra=function(){this.w().setAttribute("aria-valuenow",this.j().playbackRate())};s.s=function(){for(var a=this.j().playbackRate(),c=this.j().options().playbackRates,d=c[0],e=0;e<c.length;e++)if(c[e]>a){d=c[e];break}this.j().playbackRate(d)};function ea(a){return a.j().o&&a.j().o.featuresPlaybackRate&&a.j().options().playbackRates&&0<a.j().options().playbackRates.length}s.bd=function(){ea(this)?this.r("vjs-hidden"):this.n("vjs-hidden")};
s.ad=function(){ea(this)&&(this.Hc.innerHTML=this.j().playbackRate()+"x")};t.pb=t.J.extend({rc:"button",i:function(a,c){var d=this.label=c.rate,e=this.Oc=parseFloat(d,10);c.label=d;c.selected=1===e;t.J.call(this,a,c);this.c(a,"ratechange",this.update)}});t.pb.prototype.s=function(){t.J.prototype.s.call(this);this.j().playbackRate(this.Oc)};t.pb.prototype.update=function(){this.selected(this.j().playbackRate()==this.Oc)};
t.fc=t.u.extend({i:function(a,c){t.u.call(this,a,c);this.update();a.c("posterchange",t.bind(this,this.update))}});s=t.fc.prototype;s.dispose=function(){this.j().k("posterchange",this.update);t.u.prototype.dispose.call(this)};s.e=function(){var a=t.e("div",{className:"vjs-poster",tabIndex:-1});t.fd||(this.Eb=t.e("img"),a.appendChild(this.Eb));return a};s.update=function(){var a=this.j().poster();this.ga(a);a?this.b.style.display="":this.Y()};
s.ga=function(a){var c;this.Eb?this.Eb.src=a:(c="",a&&(c='url("'+a+'")'),this.b.style.backgroundImage=c)};s.s=function(){this.d.play()};t.bc=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});t.bc.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-loading-spinner"})};t.hb=t.u.extend();t.hb.prototype.e=function(){return t.u.prototype.e.call(this,"div",{className:"vjs-big-play-button",innerHTML:'<span aria-hidden="true"></span>',"aria-label":"play video"})};
t.hb.prototype.s=function(){this.d.play()};t.lb=t.a.extend({i:function(a,c){t.a.call(this,a,c);this.update();this.c(a,"error",this.update)}});t.lb.prototype.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-error-display"});this.v=t.e("div");a.appendChild(this.v);return a};t.lb.prototype.update=function(){this.j().error()&&(this.v.innerHTML=this.t(this.j().error().message))};
t.q=t.a.extend({i:function(a,c,d){c=c||{};c.Qc=l;t.a.call(this,a,c,d);this.featuresProgressEvents||(this.Ic=f,this.Nc=this.setInterval(function(){var a=this.j().bufferedPercent();this.vd!=a&&this.j().l("progress");this.vd=a;1===a&&this.clearInterval(this.Nc)},500));this.featuresTimeupdateEvents||(a=this.d,this.Nb=f,this.c(a,"play",this.$c),this.c(a,"pause",this.fb),this.Q("timeupdate",function(){this.featuresTimeupdateEvents=f;fa(this)}));var e;e=this.j();a=function(){if(e.controls()&&!e.usingNativeControls()){var a;
this.c("mousedown",this.s);this.c("touchstart",function(){a=this.d.userActive()});this.c("touchmove",function(){a&&this.j().reportUserActivity()});this.c("touchend",function(a){a.preventDefault()});G(this);this.c("tap",this.ce)}};this.H(a);this.c(e,"controlsenabled",a);this.c(e,"controlsdisabled",this.he);this.H(function(){this.networkState&&0<this.networkState()&&this.j().l("loadstart")})}});s=t.q.prototype;
s.he=function(){this.k("tap");this.k("touchstart");this.k("touchmove");this.k("touchleave");this.k("touchcancel");this.k("touchend");this.k("click");this.k("mousedown")};s.s=function(a){0===a.button&&this.j().controls()&&(this.j().paused()?this.j().play():this.j().pause())};s.ce=function(){this.j().userActive(!this.j().userActive())};function fa(a){a.Nb=l;a.fb();a.k("play",a.$c);a.k("pause",a.fb)}s.$c=function(){this.tc&&this.fb();this.tc=this.setInterval(function(){this.j().l("timeupdate")},250)};
s.fb=function(){this.clearInterval(this.tc);this.j().l("timeupdate")};s.dispose=function(){this.Ic&&(this.Ic=l,this.clearInterval(this.Nc));this.Nb&&fa(this);t.a.prototype.dispose.call(this)};s.Tb=function(){this.Nb&&this.j().l("timeupdate")};s.Tc=m();t.q.prototype.featuresVolumeControl=f;t.q.prototype.featuresFullscreenResize=l;t.q.prototype.featuresPlaybackRate=l;t.q.prototype.featuresProgressEvents=l;t.q.prototype.featuresTimeupdateEvents=l;
t.q.dd=function(a){a.Rb=function(c){var d,e=a.Vc;e||(e=a.Vc=[]);d===b&&(d=e.length);e.splice(d,0,c)};a.Rc=function(c){for(var d=a.Vc||[],e,g=0;g<d.length;g++)if(e=d[g].Sa(c))return d[g];return k};a.oc=function(c){var d=a.Rc(c);return d?d.Sa(c):""};a.prototype.Uc=function(c){var d=a.Rc(c);this.Cb();this.k("dispose",this.Cb);this.sc=c;this.Ub=d.Ib(c,this);this.c("dispose",this.Cb)};a.prototype.Cb=function(){this.Ub&&this.Ub.dispose&&this.Ub.dispose()}};
t.g=t.q.extend({i:function(a,c,d){t.q.call(this,a,c,d);for(d=t.g.mb.length-1;0<=d;d--)this.c(t.g.mb[d],this.Fd);(c=c.source)&&(this.b.currentSrc!==c.src||a.I&&3===a.I.Md)&&this.Uc(c);if(t.ic&&a.options().nativeControlsForTouch===f){var e,g,h,j;e=this;g=this.j();c=g.controls();e.b.controls=!!c;h=function(){e.b.controls=f};j=function(){e.b.controls=l};g.c("controlsenabled",h);g.c("controlsdisabled",j);c=function(){g.k("controlsenabled",h);g.k("controlsdisabled",j)};e.c("dispose",c);g.c("usingcustomcontrols",
c);g.usingNativeControls(f)}a.H(function(){this.I&&(this.m.autoplay&&this.paused())&&(delete this.I.poster,this.play())});this.Ja()}});s=t.g.prototype;s.dispose=function(){t.g.Bb(this.b);t.q.prototype.dispose.call(this)};
s.e=function(){var a=this.d,c=a.I,d;if(!c||this.movingMediaElementInDOM===l)c?(d=c.cloneNode(l),t.g.Bb(c),c=d,a.I=k):(c=t.e("video"),t.Sc(c,t.h.z(a.se||{},{id:a.id()+"_html5_api","class":"vjs-tech"}))),c.player=a,t.Jb(c,a.w());d=["autoplay","preload","loop","muted"];for(var e=d.length-1;0<=e;e--){var g=d[e],h={};"undefined"!==typeof a.m[g]&&(h[g]=a.m[g]);t.Sc(c,h)}return c};s.Fd=function(a){"error"==a.type&&this.error()?this.j().error(this.error().code):(a.bubbles=l,this.j().l(a))};s.play=function(){this.b.play()};
s.pause=function(){this.b.pause()};s.paused=function(){return this.b.paused};s.currentTime=function(){return this.b.currentTime};s.Tb=function(a){try{this.b.currentTime=a}catch(c){t.log(c,"Video is not ready. (Video.js)")}};s.duration=function(){return this.b.duration||0};s.buffered=function(){return this.b.buffered};s.volume=function(){return this.b.volume};s.oe=function(a){this.b.volume=a};s.muted=function(){return this.b.muted};s.le=function(a){this.b.muted=a};s.width=function(){return this.b.offsetWidth};
s.height=function(){return this.b.offsetHeight};s.Ha=function(){return"function"==typeof this.b.webkitEnterFullScreen&&(/Android/.test(t.N)||!/Chrome|Mac OS X 10.5/.test(t.N))?f:l};
s.xc=function(){var a=this.b;"webkitDisplayingFullscreen"in a&&this.Q("webkitbeginfullscreen",function(){this.d.isFullscreen(f);this.Q("webkitendfullscreen",function(){this.d.isFullscreen(l);this.d.l("fullscreenchange")});this.d.l("fullscreenchange")});a.paused&&a.networkState<=a.ze?(this.b.play(),this.setTimeout(function(){a.pause();a.webkitEnterFullScreen()},0)):a.webkitEnterFullScreen()};s.Gd=function(){this.b.webkitExitFullScreen()};s.src=function(a){if(a===b)return this.b.src;this.ga(a)};
s.ga=function(a){this.b.src=a};s.load=function(){this.b.load()};s.currentSrc=function(){return this.b.currentSrc};s.poster=function(){return this.b.poster};s.Tc=function(a){this.b.poster=a};s.Ga=function(){return this.b.Ga};s.ne=function(a){this.b.Ga=a};s.autoplay=function(){return this.b.autoplay};s.ie=function(a){this.b.autoplay=a};s.controls=function(){return this.b.controls};s.loop=function(){return this.b.loop};s.ke=function(a){this.b.loop=a};s.error=function(){return this.b.error};
s.seeking=function(){return this.b.seeking};s.ended=function(){return this.b.ended};s.playbackRate=function(){return this.b.playbackRate};s.me=function(a){this.b.playbackRate=a};s.networkState=function(){return this.b.networkState};t.g.isSupported=function(){try{t.A.volume=0.5}catch(a){return l}return!!t.A.canPlayType};t.q.dd(t.g);t.g.V={};
t.g.V.Sa=function(a){function c(a){try{return!!t.A.canPlayType(a)}catch(c){return""}}if(a.type)return c(a.type);a=a.src.match(/\.([^\/\?]+)(\?[^\/]+)?$/i)[1];return c("video/"+a)};t.g.V.Ib=function(a,c){c.ga(a.src)};t.g.V.dispose=m();t.g.Rb(t.g.V);t.g.xd=function(){var a=t.A.volume;t.A.volume=a/2+0.1;return a!==t.A.volume};t.g.wd=function(){var a=t.A.playbackRate;t.A.playbackRate=a/2+0.1;return a!==t.A.playbackRate};t.g.prototype.featuresVolumeControl=t.g.xd();t.g.prototype.featuresPlaybackRate=t.g.wd();
t.g.prototype.movingMediaElementInDOM=!t.kd;t.g.prototype.featuresFullscreenResize=f;t.g.prototype.featuresProgressEvents=f;var V,ga=/^application\/(?:x-|vnd\.apple\.)mpegurl/i,ha=/^video\/mp4/i;
t.g.Kc=function(){4<=t.Xb&&(V||(V=t.A.constructor.prototype.canPlayType),t.A.constructor.prototype.canPlayType=function(a){return a&&ga.test(a)?"maybe":V.call(this,a)});t.od&&(V||(V=t.A.constructor.prototype.canPlayType),t.A.constructor.prototype.canPlayType=function(a){return a&&ha.test(a)?"maybe":V.call(this,a)})};t.g.we=function(){var a=t.A.constructor.prototype.canPlayType;t.A.constructor.prototype.canPlayType=V;V=k;return a};t.g.Kc();t.g.mb="loadstart suspend abort error emptied stalled loadedmetadata loadeddata canplay canplaythrough playing waiting seeking seeked ended durationchange timeupdate progress play pause ratechange volumechange".split(" ");
t.g.Bb=function(a){if(a){a.player=k;for(a.parentNode&&a.parentNode.removeChild(a);a.hasChildNodes();)a.removeChild(a.firstChild);a.removeAttribute("src");if("function"===typeof a.load)try{a.load()}catch(c){}}};
t.f=t.q.extend({i:function(a,c,d){t.q.call(this,a,c,d);var e=c.source;d=c.parentEl;var g=this.b=t.e("div",{id:a.id()+"_temp_flash"}),h=a.id()+"_flash_api",j=a.m,j=t.h.z({readyFunction:"videojs.Flash.onReady",eventProxyFunction:"videojs.Flash.onEvent",errorEventProxyFunction:"videojs.Flash.onError",autoplay:j.autoplay,preload:j.Ga,loop:j.loop,muted:j.muted},c.flashVars),p=t.h.z({wmode:"opaque",bgcolor:"#000000"},c.params),h=t.h.z({id:h,name:h,"class":"vjs-tech"},c.attributes);e&&this.H(function(){this.Uc(e)});
t.Jb(g,d);c.startTime&&this.H(function(){this.load();this.play();this.currentTime(c.startTime)});t.jd&&this.H(function(){this.c("mousemove",function(){this.j().l({type:"mousemove",bubbles:l})})});a.c("stageclick",a.reportUserActivity);this.b=t.f.wc(c.swf,g,j,p,h)}});s=t.f.prototype;s.dispose=function(){t.q.prototype.dispose.call(this)};s.play=function(){this.b.vjs_play()};s.pause=function(){this.b.vjs_pause()};s.src=function(a){return a===b?this.currentSrc():this.ga(a)};
s.ga=function(a){a=t.Jd(a);this.b.vjs_src(a);if(this.d.autoplay()){var c=this;this.setTimeout(function(){c.play()},0)}};t.f.prototype.setCurrentTime=function(a){this.Ud=a;this.b.vjs_setProperty("currentTime",a);t.q.prototype.Tb.call(this)};t.f.prototype.currentTime=function(){return this.seeking()?this.Ud||0:this.b.vjs_getProperty("currentTime")};t.f.prototype.currentSrc=function(){return this.sc?this.sc.src:this.b.vjs_getProperty("currentSrc")};t.f.prototype.load=function(){this.b.vjs_load()};
t.f.prototype.poster=function(){this.b.vjs_getProperty("poster")};t.f.prototype.setPoster=m();t.f.prototype.buffered=function(){return t.zb(0,this.b.vjs_getProperty("buffered"))};t.f.prototype.Ha=r(l);t.f.prototype.xc=r(l);function ia(){var a=W[X],c=a.charAt(0).toUpperCase()+a.slice(1);ka["set"+c]=function(c){return this.b.vjs_setProperty(a,c)}}function la(a){ka[a]=function(){return this.b.vjs_getProperty(a)}}
var ka=t.f.prototype,W="rtmpConnection rtmpStream preload defaultPlaybackRate playbackRate autoplay loop mediaGroup controller controls volume muted defaultMuted".split(" "),ma="error networkState readyState seeking initialTime duration startOffsetTime paused played seekable ended videoTracks audioTracks videoWidth videoHeight textTracks".split(" "),X;for(X=0;X<W.length;X++)la(W[X]),ia();for(X=0;X<ma.length;X++)la(ma[X]);t.f.isSupported=function(){return 10<=t.f.version()[0]};t.q.dd(t.f);t.f.V={};
t.f.V.Sa=function(a){return!a.type?"":a.type.replace(/;.*/,"").toLowerCase()in t.f.Id?"maybe":""};t.f.V.Ib=function(a,c){c.ga(a.src)};t.f.V.dispose=m();t.f.Rb(t.f.V);t.f.Id={"video/flv":"FLV","video/x-flv":"FLV","video/mp4":"MP4","video/m4v":"MP4"};t.f.onReady=function(a){var c;if(c=(a=t.w(a))&&a.parentNode&&a.parentNode.player)a.player=c,t.f.checkReady(c.o)};t.f.checkReady=function(a){a.w()&&(a.w().vjs_getProperty?a.Ja():this.setTimeout(function(){t.f.checkReady(a)},50))};
t.f.onEvent=function(a,c){t.w(a).player.l(c)};t.f.onError=function(a,c){var d=t.w(a).player,e="FLASH: "+c;"srcnotfound"==c?d.error({code:4,message:e}):d.error(e)};
t.f.version=function(){var a="0,0,0";try{a=(new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash")).GetVariable("$version").replace(/\D+/g,",").match(/^,?(.+),?$/)[1]}catch(c){try{navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin&&(a=(navigator.plugins["Shockwave Flash 2.0"]||navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g,",").match(/^,?(.+),?$/)[1])}catch(d){}}return a.split(",")};
t.f.wc=function(a,c,d,e,g){a=t.f.Ld(a,d,e,g);a=t.e("div",{innerHTML:a}).childNodes[0];d=c.parentNode;c.parentNode.replaceChild(a,c);var h=d.childNodes[0];setTimeout(function(){h.style.display="block"},1E3);return a};
t.f.Ld=function(a,c,d,e){var g="",h="",j="";c&&t.h.X(c,function(a,c){g+=a+"="+c+"&amp;"});d=t.h.z({movie:a,flashvars:g,allowScriptAccess:"always",allowNetworking:"all"},d);t.h.X(d,function(a,c){h+='<param name="'+a+'" value="'+c+'" />'});e=t.h.z({data:a,width:"100%",height:"100%"},e);t.h.X(e,function(a,c){j+=a+'="'+c+'" '});return'<object type="application/x-shockwave-flash" '+j+">"+h+"</object>"};t.f.qe={"rtmp/mp4":"MP4","rtmp/flv":"FLV"};t.f.Le=function(a,c){return a+"&"+c};
t.f.pe=function(a){var c={qc:"",Zc:""};if(!a)return c;var d=a.indexOf("&"),e;-1!==d?e=d+1:(d=e=a.lastIndexOf("/")+1,0===d&&(d=e=a.length));c.qc=a.substring(0,d);c.Zc=a.substring(e,a.length);return c};t.f.Rd=function(a){return a in t.f.qe};t.f.qd=/^rtmp[set]?:\/\//i;t.f.Qd=function(a){return t.f.qd.test(a)};t.f.Sb={};t.f.Sb.Sa=function(a){return t.f.Rd(a.type)||t.f.Qd(a.src)?"maybe":""};t.f.Sb.Ib=function(a,c){var d=t.f.pe(a.src);c.Je(d.qc);c.Ke(d.Zc)};t.f.Rb(t.f.Sb);
t.pd=t.a.extend({i:function(a,c,d){t.a.call(this,a,c,d);if(!a.m.sources||0===a.m.sources.length){c=0;for(d=a.m.techOrder;c<d.length;c++){var e=t.ba(d[c]),g=window.videojs[e];if(g&&g.isSupported()){Q(a,e);break}}}else a.src(a.m.sources)}});t.Player.prototype.textTracks=function(){return this.Ia=this.Ia||[]};
function na(a,c,d,e,g){var h=a.Ia=a.Ia||[];g=g||{};g.kind=c;g.label=d;g.language=e;c=t.ba(c||"subtitles");var j=new window.videojs[c+"Track"](a,g);h.push(j);j.Ab()&&a.H(function(){this.setTimeout(function(){Y(j.j(),j.id())},0)})}function Y(a,c,d){for(var e=a.Ia,g=0,h=e.length,j,p;g<h;g++)j=e[g],j.id()===c?(j.show(),p=j):d&&(j.L()==d&&0<j.mode())&&j.disable();(c=p?p.L():d?d:l)&&a.l(c+"trackchange")}
t.B=t.a.extend({i:function(a,c){t.a.call(this,a,c);this.K=c.id||"vjs_"+c.kind+"_"+c.language+"_"+t.p++;this.Wc=c.src;this.Cd=c["default"]||c.dflt;this.ue=c.title;this.Ya=c.srclang;this.Sd=c.label;this.ca=[];this.vb=[];this.oa=this.qa=0;a.c("dispose",t.bind(this,this.vc,this.K))}});s=t.B.prototype;s.L=n("G");s.src=n("Wc");s.Ab=n("Cd");s.title=n("ue");s.language=n("Ya");s.label=n("Sd");s.yd=n("ca");s.rd=n("vb");s.readyState=n("qa");s.mode=n("oa");
s.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-"+this.G+" vjs-text-track"})};s.show=function(){oa(this);this.oa=2;t.a.prototype.show.call(this)};s.Y=function(){oa(this);this.oa=1;t.a.prototype.Y.call(this)};s.disable=function(){2==this.oa&&this.Y();this.vc();this.oa=0};function oa(a){0===a.qa&&a.load();0===a.oa&&(a.d.c("timeupdate",t.bind(a,a.update,a.K)),a.d.c("ended",t.bind(a,a.reset,a.K)),("captions"===a.G||"subtitles"===a.G)&&a.d.ma("textTrackDisplay").U(a))}
s.vc=function(){this.d.k("timeupdate",t.bind(this,this.update,this.K));this.d.k("ended",t.bind(this,this.reset,this.K));this.reset();this.d.ma("textTrackDisplay").removeChild(this)};
s.load=function(){0===this.qa&&(this.qa=1,t.ye(this.Wc,t.bind(this,function(a,c,d){if(a)this.error=a,this.qa=3,this.l("error");else{var e,g;a=d.split("\n");c="";d=1;for(var h=a.length;d<h;d++)if(c=t.trim(a[d])){-1==c.indexOf("--\x3e")?(e=c,c=t.trim(a[++d])):e=this.ca.length;e={id:e,index:this.ca.length};g=c.split(/[\t ]+/);e.startTime=pa(g[0]);e.Aa=pa(g[2]);for(g=[];a[++d]&&(c=t.trim(a[d]));)g.push(c);e.text=g.join("<br/>");this.ca.push(e)}this.qa=2;this.l("loaded")}})))};
function pa(a){var c=a.split(":");a=0;var d,e,g;3==c.length?(d=c[0],e=c[1],c=c[2]):(d=0,e=c[0],c=c[1]);c=c.split(/\s+/);c=c.splice(0,1)[0];c=c.split(/\.|,/);g=parseFloat(c[1]);c=c[0];a+=3600*parseFloat(d);a+=60*parseFloat(e);a+=parseFloat(c);g&&(a+=g/1E3);return a}
s.update=function(){if(0<this.ca.length){var a=this.d.options().trackTimeOffset||0,a=this.d.currentTime()+a;if(this.Qb===b||a<this.Qb||this.Za<=a){var c=this.ca,d=this.d.duration(),e=0,g=l,h=[],j,p,q,w;a>=this.Za||this.Za===b?w=this.Fb!==b?this.Fb:0:(g=f,w=this.Mb!==b?this.Mb:c.length-1);for(;;){q=c[w];if(q.Aa<=a)e=Math.max(e,q.Aa),q.Qa&&(q.Qa=l);else if(a<q.startTime){if(d=Math.min(d,q.startTime),q.Qa&&(q.Qa=l),!g)break}else g?(h.splice(0,0,q),p===b&&(p=w),j=w):(h.push(q),j===b&&(j=w),p=w),d=Math.min(d,
q.Aa),e=Math.max(e,q.startTime),q.Qa=f;if(g)if(0===w)break;else w--;else if(w===c.length-1)break;else w++}this.vb=h;this.Za=d;this.Qb=e;this.Fb=j;this.Mb=p;j=this.vb;p="";a=0;for(c=j.length;a<c;a++)p+='<span class="vjs-tt-cue">'+j[a].text+"</span>";this.b.innerHTML=p;this.l("cuechange")}}};s.reset=function(){this.Za=0;this.Qb=this.d.duration();this.Mb=this.Fb=0};t.Zb=t.B.extend();t.Zb.prototype.G="captions";t.hc=t.B.extend();t.hc.prototype.G="subtitles";t.$b=t.B.extend();t.$b.prototype.G="chapters";
t.jc=t.a.extend({i:function(a,c,d){t.a.call(this,a,c,d);if(a.m.tracks&&0<a.m.tracks.length){c=this.d;a=a.m.tracks;for(var e=0;e<a.length;e++)d=a[e],na(c,d.kind,d.label,d.language,d)}}});t.jc.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-text-track-display"})};t.aa=t.J.extend({i:function(a,c){var d=this.ha=c.track;c.label=d.label();c.selected=d.Ab();t.J.call(this,a,c);this.c(a,d.L()+"trackchange",this.update)}});
t.aa.prototype.s=function(){t.J.prototype.s.call(this);Y(this.d,this.ha.K,this.ha.L())};t.aa.prototype.update=function(){this.selected(2==this.ha.mode())};t.ob=t.aa.extend({i:function(a,c){c.track={L:function(){return c.kind},j:a,label:function(){return c.kind+" off"},Ab:r(l),mode:r(l)};t.aa.call(this,a,c);this.selected(f)}});t.ob.prototype.s=function(){t.aa.prototype.s.call(this);Y(this.d,this.ha.K,this.ha.L())};
t.ob.prototype.update=function(){for(var a=this.d.textTracks(),c=0,d=a.length,e,g=f;c<d;c++)e=a[c],e.L()==this.ha.L()&&2==e.mode()&&(g=l);this.selected(g)};t.T=t.M.extend({i:function(a,c){t.M.call(this,a,c);1>=this.P.length&&this.Y()}});t.T.prototype.ya=function(){var a=[],c;a.push(new t.ob(this.d,{kind:this.G}));for(var d=0;d<this.d.textTracks().length;d++)c=this.d.textTracks()[d],c.L()===this.G&&a.push(new t.aa(this.d,{track:c}));return a};
t.Ka=t.T.extend({i:function(a,c,d){t.T.call(this,a,c,d);this.b.setAttribute("aria-label","Captions Menu")}});t.Ka.prototype.G="captions";t.Ka.prototype.wa="Captions";t.Ka.prototype.className="vjs-captions-button";t.Pa=t.T.extend({i:function(a,c,d){t.T.call(this,a,c,d);this.b.setAttribute("aria-label","Subtitles Menu")}});t.Pa.prototype.G="subtitles";t.Pa.prototype.wa="Subtitles";t.Pa.prototype.className="vjs-subtitles-button";
t.La=t.T.extend({i:function(a,c,d){t.T.call(this,a,c,d);this.b.setAttribute("aria-label","Chapters Menu")}});s=t.La.prototype;s.G="chapters";s.wa="Chapters";s.className="vjs-chapters-button";s.ya=function(){for(var a=[],c,d=0;d<this.d.textTracks().length;d++)c=this.d.textTracks()[d],c.L()===this.G&&a.push(new t.aa(this.d,{track:c}));return a};
s.za=function(){for(var a=this.d.textTracks(),c=0,d=a.length,e,g,h=this.P=[];c<d;c++)if(e=a[c],e.L()==this.G)if(0===e.readyState())e.load(),e.c("loaded",t.bind(this,this.za));else{g=e;break}a=this.Da;a===b&&(a=new t.ja(this.d),a.la().appendChild(t.e("li",{className:"vjs-menu-title",innerHTML:t.ba(this.G),re:-1})));if(g){e=g.ca;for(var j,c=0,d=e.length;c<d;c++)j=e[c],j=new t.ib(this.d,{track:g,cue:j}),h.push(j),a.U(j);this.U(a)}0<this.P.length&&this.show();return a};
t.ib=t.J.extend({i:function(a,c){var d=this.ha=c.track,e=this.cue=c.cue,g=a.currentTime();c.label=e.text;c.selected=e.startTime<=g&&g<e.Aa;t.J.call(this,a,c);d.c("cuechange",t.bind(this,this.update))}});t.ib.prototype.s=function(){t.J.prototype.s.call(this);this.d.currentTime(this.cue.startTime);this.update(this.cue.startTime)};t.ib.prototype.update=function(){var a=this.cue,c=this.d.currentTime();this.selected(a.startTime<=c&&c<a.Aa)};
t.h.z(t.Ma.prototype.m.children,{subtitlesButton:{},captionsButton:{},chaptersButton:{}});
if("undefined"!==typeof window.JSON&&"function"===typeof window.JSON.parse)t.JSON=window.JSON;else{t.JSON={};var Z=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;t.JSON.parse=function(a,c){function d(a,e){var j,p,q=a[e];if(q&&"object"===typeof q)for(j in q)Object.prototype.hasOwnProperty.call(q,j)&&(p=d(q,j),p!==b?q[j]=p:delete q[j]);return c.call(a,e,q)}var e;a=String(a);Z.lastIndex=0;Z.test(a)&&(a=a.replace(Z,function(a){return"\\u"+("0000"+
a.charCodeAt(0).toString(16)).slice(-4)}));if(/^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return e=eval("("+a+")"),"function"===typeof c?d({"":e},""):e;throw new SyntaxError("JSON.parse(): invalid or malformed JSON data");}}
t.nc=function(){var a,c,d,e;a=document.getElementsByTagName("video");c=document.getElementsByTagName("audio");var g=[];if(a&&0<a.length){d=0;for(e=a.length;d<e;d++)g.push(a[d])}if(c&&0<c.length){d=0;for(e=c.length;d<e;d++)g.push(c[d])}if(g&&0<g.length){d=0;for(e=g.length;d<e;d++)if((c=g[d])&&c.getAttribute)c.player===b&&(a=c.getAttribute("data-setup"),a!==k&&videojs(c));else{t.wb();break}}else t.cd||t.wb()};t.wb=function(){setTimeout(t.nc,1)};
"complete"===document.readyState?t.cd=f:t.Q(window,"load",function(){t.cd=f});t.wb();t.ge=function(a,c){t.Player.prototype[a]=c};var qa=this;function $(a,c){var d=a.split("."),e=qa;!(d[0]in e)&&e.execScript&&e.execScript("var "+d[0]);for(var g;d.length&&(g=d.shift());)!d.length&&c!==b?e[g]=c:e=e[g]?e[g]:e[g]={}};$("videojs",t);$("_V_",t);$("videojs.options",t.options);$("videojs.players",t.Fa);$("videojs.TOUCH_ENABLED",t.ic);$("videojs.cache",t.xa);$("videojs.Component",t.a);t.a.prototype.player=t.a.prototype.j;t.a.prototype.options=t.a.prototype.options;t.a.prototype.init=t.a.prototype.i;t.a.prototype.dispose=t.a.prototype.dispose;t.a.prototype.createEl=t.a.prototype.e;t.a.prototype.contentEl=t.a.prototype.la;t.a.prototype.el=t.a.prototype.w;t.a.prototype.addChild=t.a.prototype.U;
t.a.prototype.getChild=t.a.prototype.ma;t.a.prototype.getChildById=t.a.prototype.Kd;t.a.prototype.children=t.a.prototype.children;t.a.prototype.initChildren=t.a.prototype.Dc;t.a.prototype.removeChild=t.a.prototype.removeChild;t.a.prototype.on=t.a.prototype.c;t.a.prototype.off=t.a.prototype.k;t.a.prototype.one=t.a.prototype.Q;t.a.prototype.trigger=t.a.prototype.l;t.a.prototype.triggerReady=t.a.prototype.Ja;t.a.prototype.show=t.a.prototype.show;t.a.prototype.hide=t.a.prototype.Y;
t.a.prototype.width=t.a.prototype.width;t.a.prototype.height=t.a.prototype.height;t.a.prototype.dimensions=t.a.prototype.Dd;t.a.prototype.ready=t.a.prototype.H;t.a.prototype.addClass=t.a.prototype.n;t.a.prototype.removeClass=t.a.prototype.r;t.a.prototype.buildCSSClass=t.a.prototype.S;t.a.prototype.localize=t.a.prototype.t;t.a.prototype.setInterval=t.a.prototype.setInterval;t.a.prototype.setTimeout=t.a.prototype.setTimeout;t.Player.prototype.ended=t.Player.prototype.ended;
t.Player.prototype.enterFullWindow=t.Player.prototype.yc;t.Player.prototype.exitFullWindow=t.Player.prototype.Db;t.Player.prototype.preload=t.Player.prototype.Ga;t.Player.prototype.remainingTime=t.Player.prototype.remainingTime;t.Player.prototype.supportsFullScreen=t.Player.prototype.Ha;t.Player.prototype.currentType=t.Player.prototype.zd;t.Player.prototype.requestFullScreen=t.Player.prototype.requestFullScreen;t.Player.prototype.requestFullscreen=t.Player.prototype.requestFullscreen;
t.Player.prototype.cancelFullScreen=t.Player.prototype.cancelFullScreen;t.Player.prototype.exitFullscreen=t.Player.prototype.exitFullscreen;t.Player.prototype.isFullScreen=t.Player.prototype.isFullScreen;t.Player.prototype.isFullscreen=t.Player.prototype.isFullscreen;$("videojs.MediaLoader",t.pd);$("videojs.TextTrackDisplay",t.jc);$("videojs.ControlBar",t.Ma);$("videojs.Button",t.u);$("videojs.PlayToggle",t.dc);$("videojs.FullscreenToggle",t.Na);$("videojs.BigPlayButton",t.hb);
$("videojs.LoadingSpinner",t.bc);$("videojs.CurrentTimeDisplay",t.jb);$("videojs.DurationDisplay",t.kb);$("videojs.TimeDivider",t.kc);$("videojs.RemainingTimeDisplay",t.rb);$("videojs.LiveDisplay",t.ac);$("videojs.ErrorDisplay",t.lb);$("videojs.Slider",t.R);$("videojs.ProgressControl",t.qb);$("videojs.SeekBar",t.gc);$("videojs.LoadProgressBar",t.nb);$("videojs.PlayProgressBar",t.cc);$("videojs.SeekHandle",t.Oa);$("videojs.VolumeControl",t.tb);$("videojs.VolumeBar",t.sb);$("videojs.VolumeLevel",t.lc);
$("videojs.VolumeMenuButton",t.ua);$("videojs.VolumeHandle",t.ub);$("videojs.MuteToggle",t.ka);$("videojs.PosterImage",t.fc);$("videojs.Menu",t.ja);$("videojs.MenuItem",t.J);$("videojs.MenuButton",t.M);$("videojs.PlaybackRateMenuButton",t.ec);t.M.prototype.createItems=t.M.prototype.ya;t.T.prototype.createItems=t.T.prototype.ya;t.La.prototype.createItems=t.La.prototype.ya;$("videojs.SubtitlesButton",t.Pa);$("videojs.CaptionsButton",t.Ka);$("videojs.ChaptersButton",t.La);
$("videojs.MediaTechController",t.q);t.q.prototype.featuresVolumeControl=t.q.prototype.Ge;t.q.prototype.featuresFullscreenResize=t.q.prototype.Ce;t.q.prototype.featuresPlaybackRate=t.q.prototype.De;t.q.prototype.featuresProgressEvents=t.q.prototype.Ee;t.q.prototype.featuresTimeupdateEvents=t.q.prototype.Fe;t.q.prototype.setPoster=t.q.prototype.Tc;$("videojs.Html5",t.g);t.g.Events=t.g.mb;t.g.isSupported=t.g.isSupported;t.g.canPlaySource=t.g.oc;t.g.patchCanPlayType=t.g.Kc;t.g.unpatchCanPlayType=t.g.we;
t.g.prototype.setCurrentTime=t.g.prototype.Tb;t.g.prototype.setVolume=t.g.prototype.oe;t.g.prototype.setMuted=t.g.prototype.le;t.g.prototype.setPreload=t.g.prototype.ne;t.g.prototype.setAutoplay=t.g.prototype.ie;t.g.prototype.setLoop=t.g.prototype.ke;t.g.prototype.enterFullScreen=t.g.prototype.xc;t.g.prototype.exitFullScreen=t.g.prototype.Gd;t.g.prototype.playbackRate=t.g.prototype.playbackRate;t.g.prototype.setPlaybackRate=t.g.prototype.me;$("videojs.Flash",t.f);t.f.isSupported=t.f.isSupported;
t.f.canPlaySource=t.f.oc;t.f.onReady=t.f.onReady;t.f.embed=t.f.wc;t.f.version=t.f.version;$("videojs.TextTrack",t.B);t.B.prototype.label=t.B.prototype.label;t.B.prototype.kind=t.B.prototype.L;t.B.prototype.mode=t.B.prototype.mode;t.B.prototype.cues=t.B.prototype.yd;t.B.prototype.activeCues=t.B.prototype.rd;$("videojs.CaptionsTrack",t.Zb);$("videojs.SubtitlesTrack",t.hc);$("videojs.ChaptersTrack",t.$b);$("videojs.autoSetup",t.nc);$("videojs.plugin",t.ge);$("videojs.createTimeRange",t.zb);
$("videojs.util",t.Z);t.Z.mergeOptions=t.Z.Ea;t.addLanguage=t.sd;})();

},{}],6:[function(require,module,exports){
/**
 * Main controller
 *    Dependency: video-js.css with fonts, movie-crop.css with images
 */

var bldMediaSpec = require('./models/media-spec');
var bldVwmMediaSpec = require('./viewmodels/media-spec');

exports.init = function(sources, frames, cropLength, handleRange) {
  var mediaSpec = bldMediaSpec.init({
    arr_media_file: sources,
    arr_media_frame: frames,
    crop_length: cropLength,
    handleRange: handleRange
  });

  var vwmMediaSpec = bldVwmMediaSpec.init(mediaSpec);
  vwmMediaSpec.gnrt();
  return vwmMediaSpec.block;
};

module.exports = exports;

},{"./models/media-spec":9,"./viewmodels/media-spec":10}],7:[function(require,module,exports){
/**
 * Media file
 * @module
 */

/**
 * Media file
 * @constructor
 */
var Mdl = function(data){
  Object.keys(data).forEach(this.buildProp.bind(this, data));
  
  
};

/**
 * Build a prop for a model
 */
Mdl.prototype.buildProp = function(data, propKey){
  this[propKey] = data[propKey];
};

/**
 * Create an instance
 */
exports.init = function(){
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{}],8:[function(require,module,exports){
/**
 * Media frame
 * @module
 */

/**
 * Media frame
 * @constructor
 */
var Mdl = function(data){
  Object.keys(data).forEach(this.buildProp.bind(this, data));
  
  
};

/**
 * Build a prop for a model
 */
Mdl.prototype.buildProp = function(data, propKey){
  this[propKey] = data[propKey];
};

/**
 * Create an instance
 */
exports.init = function(){
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{}],9:[function(require,module,exports){
/**
 * Media spec
 * @module
 */

var mdlMediaFile = require('./media-file');
var mdlMediaFrame = require('./media-frame');

/**
 * Media spec
 * @constructor
 */
var Mdl = function(data) {
  Object.keys(data).forEach(this.buildProp.bind(this, data));

  /**
   * Length to crop, seconds (double)
   * @type {Number}
   */
  this.crop_length = data.crop_length;

  /**
   * Callback, returns (start, stop)
   * @type {Function}
   */
  this.handleRange = data.handleRange;

  /** 
   * Media files with url and type of a file
   * @type {Array}
   */
  this.arr_media_file = data.arr_media_file.map(this.buildMediaFile.bind(this));

  /**
   * Frames for a video: images
   * @type {Array}
   */
  this.arr_media_frame = data.arr_media_frame.map(this.buildMediaFrame.bind(this));
};

/**
 * Build a prop for a model
 */
Mdl.prototype.buildProp = function(data, propKey) {
  this[propKey] = data[propKey];
};

/**
 * Init a media file
 */
Mdl.prototype.buildMediaFile = function(item) {
  return mdlMediaFile.init(item);
};

/**
 * Init a media frame
 */
Mdl.prototype.buildMediaFrame = function(item) {
  return mdlMediaFrame.init(item);
};

/**
 * Get array of urls of frames
 * @returns {Array}
 */
Mdl.prototype.getFrameUrls = function() {
  return this.arr_media_frame.map(function(mediaFrame) {
    return mediaFrame.url;
  });
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{"./media-file":7,"./media-frame":8}],10:[function(require,module,exports){
/**
 * Viewmodel media spec
 * @module
 */

var vjsHelper = require('video.js/dist/video-js/video.js');
// adds rangeSlider to jquery
require('movie-crop-slider/js/ion.rangeSlider.js');

var mdlMovieCrop = require('movie-crop-slider/js/movie.crop.js');

/**
 * Viewmodel media spec
 * @constructor
 */
var Mdl = function(model) {
  this.getModel = function() {
    return model;
  };

  /**
   * Block: parent of a whole view
   */
  this.block = document.createElement('div');

  /**
   * A wrap of a cutter
   */
  this.cutterWrap = document.createElement('div');

  /**
   * Video js player
   *    generated dynamically
   */
  this.vdo = null;

  this.fillConstPart();
};

/**
 * Fill constant part of a view
 */
Mdl.prototype.fillConstPart = function() {
  this.block.appendChild(this.cutterWrap);
};

/**
 * When a range is changed
 */
Mdl.prototype.onChangeCrop = function(data) {
  this.vdo.currentTime(data.from);
  this.getModel().handleRange(data.from, data.to);
};

Mdl.prototype.runCutter = function() {
  var duration = this.vdo.duration();
  var wrap = document.createElement('div');

  var movieCrop = mdlMovieCrop.init(wrap, {
    // контекст, если нужен
    scope: null,

    // Кадры из видео, может быть любое кол-во, но оптимально 8-14
    frames: this.getModel().getFrameUrls(),

    // информация о размере видео (в секундах)
    full_length: duration,
    crop_length: this.getModel().crop_length,
    start_from: 0,

    change: this.onChangeCrop.bind(this)
  });
  movieCrop.init();

  this.cutterWrap.appendChild(wrap);
};

/**
 * After metadata is ready
 */
Mdl.prototype.handleMetaReady = function() {
  this.runCutter();
};

/**
 * Generate a view
 */
Mdl.prototype.gnrt = function() {
  var videoElem = document.createElement('video');
  var videoSource = document.createElement('source');
  // get first media_file only
  var mediaFile = this.getModel().arr_media_file[0];
  videoSource.src = mediaFile.url;
  videoSource.type = mediaFile.mime_type;
  videoElem.className = videoElem.className + ' video-js vjs-default-skin';
  videoElem.appendChild(videoSource);
  var playerWrap = document.createElement('div');
  playerWrap.appendChild(videoElem);
  this.block.appendChild(playerWrap);


  var ths = this;
  // Player builds using videojs and inserted a link
  vjsHelper(videoElem, {
    width: '100%',
    height: '100%',
    controls: true,
    preload: true
  }, function() {
    // this = video elem created by video js
    ths.vdo = this;
    this.one('loadedmetadata', ths.handleMetaReady.bind(ths));
  });
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{"movie-crop-slider/js/ion.rangeSlider.js":3,"movie-crop-slider/js/movie.crop.js":4,"video.js/dist/video-js/video.js":5}]},{},[1]);
